package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.News;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/6/2018.
 */

public class ListNewsResponse {

    @SerializedName("Results")
    @Expose
    public List<News> news;

    public List<News> getNews() {
        return news;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }
}
