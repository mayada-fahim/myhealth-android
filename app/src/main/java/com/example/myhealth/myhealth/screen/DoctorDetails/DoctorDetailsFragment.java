package com.example.myhealth.myhealth.screen.DoctorDetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.News;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.CircleTransform;
import com.example.myhealth.myhealth.screen.Constants;
import com.example.myhealth.myhealth.screen.NewsDetails.NewsDetailsFragment;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

/**
 * Created by Mayada Fahim on 6/10/2018.
 */

public class DoctorDetailsFragment extends Fragment {

    private User doctor;
    private ImageView doctorImage;
    private TextView name;
    private TextView speciality;
    private TextView bio;
    private TextView address;
    private TextView phone;
    private TextView mobile;
    private LinearLayout mobileLayout;
    private LinearLayout bioLayout;
    private LinearLayout locationLayout;
    private LinearLayout phoneLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static DoctorDetailsFragment newInstance(String doctor) {
        DoctorDetailsFragment doctorDetailsFragment = new DoctorDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("doctor", doctor);
        doctorDetailsFragment.setArguments(bundle);

        return doctorDetailsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.doctor_details_fragment, container, false);
        String userStr = getArguments().getString("doctor");
        doctor = new Gson().fromJson(userStr, User.class);
        doctorImage = (ImageView) rootView.findViewById(R.id.doctorImage);
        name = (TextView) rootView.findViewById(R.id.doctorName);
        speciality = (TextView) rootView.findViewById(R.id.speciality);
        bio = (TextView) rootView.findViewById(R.id.doctorBio);
        address = (TextView) rootView.findViewById(R.id.doctorAddress);
        phone = (TextView) rootView.findViewById(R.id.doctorPhone);
        mobile = (TextView) rootView.findViewById(R.id.doctorMobile);
        locationLayout = (LinearLayout) rootView.findViewById(R.id.locationLayout);
        phoneLayout = (LinearLayout) rootView.findViewById(R.id.phoneLayout);
        mobileLayout = (LinearLayout) rootView.findViewById(R.id.mobileLayout);
        bioLayout = (LinearLayout) rootView.findViewById(R.id.bioLayout);

        name.setText(doctor.getName());
        if(doctor.getSpeciality() != null) {
            speciality.setText(doctor.getSpeciality().getName());
        }
        if(doctor.getBio() != null) {
            bio.setText(doctor.getBio());
        }else{
            bioLayout.setVisibility(View.GONE);
        }
        if(doctor.getClinicLocation() != null) {
            address.setText(doctor.getClinicLocation());
        }else{
            locationLayout.setVisibility(View.GONE);
        }
        if(doctor.getClinicPhone() != null) {
            phone.setText(doctor.getClinicPhone());
        }else{
            phoneLayout.setVisibility(View.GONE);
        }
        if(doctor.getMobileNumber() != null) {
            mobile.setText(doctor.getMobileNumber());
        }else {
            mobileLayout.setVisibility(View.GONE);
        }

        if (doctor.getProfileImage() != null) {
            Picasso.with(getContext()).load(Constants.BASE_URL + doctor.getProfileImage()).transform(new CircleTransform())
                    .into(doctorImage);
        }
        return rootView;

    }

}
