package com.example.myhealth.myhealth.screen.News;

import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.News;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/6/2018.
 */

public interface NewsView {
    public void setNews(List<News> news);
    public void error(String message);
    public void success();
}
