package com.example.myhealth.myhealth.screen.DiseaseDetails;

import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Disease;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */

public interface DiseaseDetailsView {
    public void setDisease(Disease disease);

    public void error(String message);

    public void success();

    public void setArticles(List<Article> articles);
}
