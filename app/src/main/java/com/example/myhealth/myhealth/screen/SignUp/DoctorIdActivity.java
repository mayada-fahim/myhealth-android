package com.example.myhealth.myhealth.screen.SignUp;

import android.os.Bundle;
import android.view.WindowManager;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.BaseActivity;

/**
 * Created by Mayada Fahim on 4/29/2018.
 */

public class DoctorIdActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_id_activity);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DoctorIdFragment doctorIdFragment = new DoctorIdFragment();
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.doctorIdActivity, doctorIdFragment, DoctorIdFragment.class.getSimpleName())
                .commit();
    }
}
