package com.example.myhealth.myhealth.screen.foodCalories;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.DummyModelCalories;
import com.example.myhealth.myhealth.screen.BaseActivity;

public class CaloriesDetailsActivity extends BaseActivity {

    public final static String CALORIES_LIST_ID = "calories_list_id";

    private RelativeLayout headerlayout;
    private ImageView imagerepresent;
    private TextView title;

    private TextView bmibody;
    private ImageView back;
    private TextView titleheader;
    private ImageView search;
    private View searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calories_details);


        bmibody = findViewById(R.id.bmi_body);
        title = findViewById(R.id.title);
        imagerepresent = findViewById(R.id.image_represent);
        headerlayout = findViewById(R.id.header_layout);

        titleheader = findViewById(R.id.headerTtitle);
        back = findViewById(R.id.back);
        search = findViewById(R.id.search);
        searchView = findViewById(R.id.searchView);
        search.setVisibility(View.GONE);
        searchView.setVisibility(View.GONE);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {


            DummyModelCalories dummyModelCalories = extras.getParcelable(CALORIES_LIST_ID);
            titleheader.setText(dummyModelCalories.getTitle());
        }


        setBackClickListener();


        CaloriesDetialsFragment caloriesDetialsFragment = new CaloriesDetialsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, caloriesDetialsFragment).commit();


    }


    private void setBackClickListener() {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(activity, FoodCaloriesActivity.class);
//                startActivity(intent);

                finish();
                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

            }
        });


    }

}
