package com.example.myhealth.myhealth.services;

import android.content.SharedPreferences;
import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.responses.AddTokenResponse;
import com.example.myhealth.myhealth.api.services.TokenService;
import com.example.myhealth.myhealth.screen.UserHelper;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";
    @SuppressWarnings("CanBeFinal")

    private String token;
    private UserHelper userHelper;

    public FirebaseIDService() {
        super();

        // userRepository = UserRepository.getInstance(UserRemoteDataSource.getInstance(), UserLocaleDataSource.getInstance());
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + token);
        SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences("myHealth", MODE_PRIVATE).edit();
        editor.putString("deviceToken", token);
        editor.apply();
        sendRegistrationToServer(token);

    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {
        // Add custom implementation, as needed.
        TokenService service = Network.getInstance().createService(TokenService.class);
        String userId;
        userHelper = new UserHelper(getApplicationContext());
        if (userHelper.isUserLoggedIn()) {
            userId = userHelper.getUserId() + "";
        } else {
            userId = null;
        }
        service.addToken(token, userId).enqueue(new Callback<AddTokenResponse>() {
            @Override
            public void onResponse(Call<AddTokenResponse> call, Response<AddTokenResponse> response) {
                Log.e(TAG, "success");
            }

            @Override
            public void onFailure(Call<AddTokenResponse> call, Throwable t) {

            }
        });
    }

    public void uploadToken(String token) {
        sendRegistrationToServer(token);
    }

}
