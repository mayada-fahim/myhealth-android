package com.example.myhealth.myhealth.screen.SignIn;

import com.example.myhealth.myhealth.api.models.User;

/**
 * Created by Mayada Fahim on 4/28/2018.
 */

public interface SignInView {

    public void error(String message);
    public void success();
    public void setUser(User user);
}
