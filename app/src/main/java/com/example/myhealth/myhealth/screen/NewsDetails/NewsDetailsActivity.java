package com.example.myhealth.myhealth.screen.NewsDetails;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.News;
import com.example.myhealth.myhealth.screen.ArticleDetails.ArticleDetailsFragment;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.google.gson.Gson;

/**
 * Created by Mayada Fahim on 5/6/2018.
 */

public class NewsDetailsActivity extends BaseActivity {

    private TextView title;
    private ImageView back;
    private ImageView search;
    private News news;
    private View backView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_details_activity);
        title = (TextView)findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        backView = (View) findViewById(R.id.searchView);
        search.setVisibility(View.GONE);
        backView.setVisibility(View.GONE);
        String jsonMyObject;
        Bundle extras = getIntent().getExtras();
        jsonMyObject = extras.getString("news");
        news = new Gson().fromJson(jsonMyObject, News.class);
        NewsDetailsFragment newsDetailsFragment = NewsDetailsFragment.newInstance(new Gson().toJson(news)); ;
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.newsDetailsActivity, newsDetailsFragment, NewsDetailsFragment.class.getSimpleName())
                .commit();;
        title.setText("News");
        setBackClickListener();
    }

    private void  setBackClickListener()
    {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
    }
}
