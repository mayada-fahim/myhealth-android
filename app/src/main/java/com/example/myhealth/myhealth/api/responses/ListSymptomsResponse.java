package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.Category;
import com.example.myhealth.myhealth.api.models.Symptom;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */

public class ListSymptomsResponse {

    @SerializedName("Results")
    @Expose
    public List<Symptom> symptoms = null;


    public List<Symptom> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(List<Symptom> symptoms) {
        this.symptoms = symptoms;
    }
}
