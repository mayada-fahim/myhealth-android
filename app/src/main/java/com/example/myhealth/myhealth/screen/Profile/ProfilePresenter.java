package com.example.myhealth.myhealth.screen.Profile;

import android.content.Context;
import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.api.responses.ListArticlesResponse;
import com.example.myhealth.myhealth.api.responses.SignInResponse;
import com.example.myhealth.myhealth.api.responses.SignUpResponse;
import com.example.myhealth.myhealth.api.services.ArticleService;
import com.example.myhealth.myhealth.api.services.UserService;
import com.example.myhealth.myhealth.screen.Article.ArticleView;
import com.example.myhealth.myhealth.screen.Constants;
import com.example.myhealth.myhealth.screen.UserHelper;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */

public class ProfilePresenter {


    private ProfileView mView;

    public ProfilePresenter(ProfileView profileView) {
        mView = profileView;
    }

    public void getUser(Context context) {
        UserHelper userHelper = new UserHelper(context);
        UserService service = Network.getInstanceWithHelper(userHelper).createService(UserService.class);
        service.getCurrentUser().enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                SignInResponse responseBody = response.body();
                if (responseBody != null) {
                    responseBody.getUser();
                    mView.setUser(responseBody.getUser());
                }


            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                Log.e("here6", t.getMessage());
            }
        });
    }

    public void updateUser(Context context, User user, String file) {
        if (file != null) {
            uploadFile(user, file, context );
        } else {
            sendUser(user, context);
        }

    }

    private void sendUser(User user, Context context)
    {
        UserHelper userHelper = new UserHelper(context);
        UserService service = Network.getInstanceWithHelper(userHelper).createService(UserService.class);
        service.update(user.getName(), user.getMobileNumber(), user.getGender(), user.getImage(), user.getSpecialityId(),
                user.getJobId(),user.getProfileImage(), user.getCountry(),user.getBirthDate(), user.getEmail(),
                user.getPassword(), user.getBio()).enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                SignInResponse responseBody = response.body();
                if (responseBody != null) {
                    responseBody.getUser();
                    mView.success();
                }


            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                Log.e("here6", t.getMessage());
            }
        });
    }

    private void uploadFile(final User user, String filePath, final Context context) {
        final File file = new File(filePath);

        if (validateFileSize(file.length())) {


            Ion.with(context)
                    .load(Constants.UPLOAD_URL)
                    .setMultipartFile("file", "*", file)
                    .asJsonObject().
                    setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            Log.e("here", result.get("url").toString().replaceAll("\"", ""));
                            user.setProfileImage(result.get("url").toString().replaceAll("\"", ""));
                            sendUser(user, context);
                        }


                    });


        }
    }

    private Boolean validateFileSize(Long fileLength) {
        long fileSizeInMB = fileLength / 1048576;
        return fileSizeInMB <= 10;
    }
}
