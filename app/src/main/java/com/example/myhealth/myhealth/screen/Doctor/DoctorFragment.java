package com.example.myhealth.myhealth.screen.Doctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.AToZ.DiseaseAdapter;
import com.example.myhealth.myhealth.screen.Clinic.CategoryAdapter;
import com.example.myhealth.myhealth.screen.Clinic.ClinicFragment;
import com.example.myhealth.myhealth.screen.Clinic.ClinicPresenter;
import com.example.myhealth.myhealth.screen.DoctorDetails.DoctorDetailsActivity;
import com.example.myhealth.myhealth.screen.NewsDetails.NewsDetailsActivity;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */

public class DoctorFragment extends Fragment implements DoctorAdapter.ItemClickListener, DoctorView {

    private RecyclerView recyclerView;
    private DoctorAdapter doctorAdapter;
    private DoctorPresenter doctorPresenter;
    private int page = 1;
    private  String text = "";
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static DoctorFragment newInstance() {
        return new DoctorFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.doctors_fragment, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.doctors);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setOnScrollListener(new DiseaseAdapter.EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page = current_page;
                getDoctors(text, page);
            }
        });
        doctorAdapter = new DoctorAdapter(getActivity());
        doctorPresenter = new DoctorPresenter(this);
        recyclerView.setAdapter(doctorAdapter);
        getDoctors("", page);
        return rootView;
    }

    public void getDoctors(String text, int page) {
        progressBar.setVisibility(View.VISIBLE);
        this.page = page;
        doctorPresenter.getDoctors(page, text);
    }

    public void clearData() {
        doctorAdapter.clearData();
    }

    @Override
    public void setDoctors(List<User> doctors) {
        progressBar.setVisibility(View.GONE);
        doctorAdapter.setDoctors(doctors);
        doctorAdapter.setClickListener(this);
    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), DoctorDetailsActivity.class);
        intent.putExtra("doctor", new Gson().toJson(doctorAdapter.getItem(position)));
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}
