package com.example.myhealth.myhealth.screen.Doctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.Clinic.ClinicFragment;
import com.example.myhealth.myhealth.screen.home.HomeActivity;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */

public class DoctorActivity extends BaseActivity {

    private TextView title;
    private ImageView back;
    private ImageView search;
    private RelativeLayout mainLayout;
    private RelativeLayout searchLayout;
    private ImageView searchBack;
    private EditText searchText;
    private DoctorFragment doctorFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctors_activity);
        title = (TextView) findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        mainLayout = (RelativeLayout) findViewById(R.id.default_layout);
        searchLayout = (RelativeLayout) findViewById(R.id.search_layout);
        searchBack = (ImageView) findViewById(R.id.back_green);
        searchText = (EditText) findViewById(R.id.search_text);
        doctorFragment = new DoctorFragment();
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.doctorsActivity, doctorFragment, DoctorFragment.class.getSimpleName())
                .commit();

        title.setText("Doctors");
        setBackClickListener();
        searchClickListener();
    }

    private void searchClickListener() {
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchLayout.setVisibility(View.VISIBLE);
                mainLayout.setVisibility(View.GONE);
            }
        });

        searchBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchLayout.setVisibility(View.GONE);
                mainLayout.setVisibility(View.VISIBLE);
                doctorFragment.clearData();
                doctorFragment.getDoctors("", 1);
                searchText.setText("");
            }
        });

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    doctorFragment.clearData();
                    doctorFragment.getDoctors(searchText.getText().toString(), 1);
                    hideKeyboardIfShown();
                    return true;
                }
                return false;
            }
        });

    }

    private void setBackClickListener() {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, HomeActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);

        this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
