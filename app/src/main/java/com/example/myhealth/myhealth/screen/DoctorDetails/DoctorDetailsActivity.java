package com.example.myhealth.myhealth.screen.DoctorDetails;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.News;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.NewsDetails.NewsDetailsFragment;
import com.google.gson.Gson;

/**
 * Created by Mayada Fahim on 6/10/2018.
 */

public class DoctorDetailsActivity extends BaseActivity {

    private TextView title;
    private ImageView back;
    private ImageView search;
    private User doctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_details_activity);
        title = (TextView)findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        search.setVisibility(View.GONE);
        String jsonMyObject;
        Bundle extras = getIntent().getExtras();
        jsonMyObject = extras.getString("doctor");
        doctor = new Gson().fromJson(jsonMyObject, User.class);
        DoctorDetailsFragment doctorDetailsFragment = DoctorDetailsFragment.newInstance(new Gson().toJson(doctor)); ;
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.doctorDetailsActivity, doctorDetailsFragment, DoctorDetailsFragment.class.getSimpleName())
                .commit();;
        title.setText("Doctor");
        setBackClickListener();
    }

    private void  setBackClickListener()
    {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
    }
}
