package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.api.models.Video;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */

public class ListDoctorsResponse {

    @SerializedName("Results")
    @Expose
    public List<User> doctors;

    public List<User> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<User> doctors) {
        this.doctors = doctors;
    }
}
