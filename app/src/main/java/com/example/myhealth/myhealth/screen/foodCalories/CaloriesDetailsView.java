package com.example.myhealth.myhealth.screen.foodCalories;


import com.example.myhealth.myhealth.api.models.DummyModelDetailsCalories;

import java.util.List;

/**
 * Created by Hossam Elsawy on 02/06/18.
 */
public interface CaloriesDetailsView {

    void setCaloriesDetials(List<DummyModelDetailsCalories> dummyModelCalories);
}
