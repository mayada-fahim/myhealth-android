package com.example.myhealth.myhealth.api.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hossam Elsawy on 29/05/18.
 */
public class BMIRepresent implements Parcelable {

    public static final Creator<BMIRepresent> CREATOR = new Creator<BMIRepresent>() {
        @Override
        public BMIRepresent createFromParcel(Parcel in) {
            return new BMIRepresent(in);
        }

        @Override
        public BMIRepresent[] newArray(int size) {
            return new BMIRepresent[size];
        }
    };
    private String title;
    private String body;
    private Integer ImageRepresent;

    public BMIRepresent(String title, String body, Integer imageRepresent) {
        this.title = title;
        this.body = body;
        ImageRepresent = imageRepresent;
    }

    protected BMIRepresent(Parcel in) {
        title = in.readString();
        body = in.readString();
        if (in.readByte() == 0) {
            ImageRepresent = null;
        } else {
            ImageRepresent = in.readInt();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getImageRepresent() {
        return ImageRepresent;
    }

    public void setImageRepresent(Integer imageRepresent) {
        ImageRepresent = imageRepresent;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(body);
        if (ImageRepresent == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(ImageRepresent);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
