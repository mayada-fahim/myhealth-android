package com.example.myhealth.myhealth.screen.foodCalories;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.DummyModelCalories;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hossam Elsawy on 30/05/18.
 */
public class CaloriesListPresenter {

    CaloriesListView mView;
    List<DummyModelCalories> dummyModelCalories = new ArrayList<>();


    public CaloriesListPresenter(CaloriesListView caloriesListView) {
        this.mView = caloriesListView;

    }

    public void getCaloriesModel() {


        DummyModelCalories dummyModelCalories1 = new DummyModelCalories();

        dummyModelCalories1.setImage(R.drawable.c_l_bakery);
        dummyModelCalories1.setTitle("Bakery Products");

        DummyModelCalories dummyModelCalories2 = new DummyModelCalories();

        dummyModelCalories2.setImage(R.drawable.c_l_breakfast);
        dummyModelCalories2.setTitle("Breakfast Cereals");
        DummyModelCalories dummyModelCalories3 = new DummyModelCalories();

        dummyModelCalories3.setImage(R.drawable.c_l_cheese);
        dummyModelCalories3.setTitle("Cheese ");

        dummyModelCalories.add(dummyModelCalories1);
        dummyModelCalories.add(dummyModelCalories2);
        dummyModelCalories.add(dummyModelCalories3);


        mView.setCalories(dummyModelCalories);


    }
}
