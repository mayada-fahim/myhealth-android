package com.example.myhealth.myhealth.screen.SignIn;

import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.api.responses.AddTokenResponse;
import com.example.myhealth.myhealth.api.responses.SignInResponse;
import com.example.myhealth.myhealth.api.services.TokenService;
import com.example.myhealth.myhealth.api.services.UserService;
import com.example.myhealth.myhealth.screen.UserHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 4/28/2018.
 */

public class SignInPresenter {

    private SignInView mView;

    public SignInPresenter(SignInView signInView) {
        mView = signInView;
    }

    public void signIn(String email, String password) {
        UserService service = Network.getInstance().createService(UserService.class);

        service.signIn(email, password).enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                SignInResponse responseBody = response.body();
                User user = new User();
                Log.e("here", responseBody + "");
                if (responseBody != null) {
                    String error = responseBody.getError();
                    Log.e("here", error + "");
                    if (error != null && error.equals("invalid_credentials")) {
                        mView.error("Wrong Email or Password");
                    } else {
                        user = responseBody.getUser();
                        mView.setUser(user);
                    }
                }else {
                    mView.error("Wrong Email or Password");
                }

            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {

            }
        });
    }

    public void sendToken(String token , String userId)
    {
        TokenService service = Network.getInstance().createService(TokenService.class);

        service.addToken(token, userId).enqueue(new Callback<AddTokenResponse>() {
            @Override
            public void onResponse(Call<AddTokenResponse> call, Response<AddTokenResponse> response) {
                Log.e("success", "success");
            }

            @Override
            public void onFailure(Call<AddTokenResponse> call, Throwable t) {

            }
        });
    }
}
