package com.example.myhealth.myhealth.api.services;

import com.example.myhealth.myhealth.api.responses.ListArticlesResponse;
import com.example.myhealth.myhealth.api.responses.ListCategoriesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public interface ArticleService {

    @GET("articles/{disease_id}/{page}")
    Call<ListArticlesResponse> getArticles(@Path("disease_id") int diseaseId, @Path("page") int page);
}
