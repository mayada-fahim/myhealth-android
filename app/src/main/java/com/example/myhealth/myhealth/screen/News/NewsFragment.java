package com.example.myhealth.myhealth.screen.News;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.News;
import com.example.myhealth.myhealth.screen.Article.ArticleAdapter;
import com.example.myhealth.myhealth.screen.Article.ArticleFragment;
import com.example.myhealth.myhealth.screen.Article.ArticlePresenter;
import com.example.myhealth.myhealth.screen.ArticleDetails.ArticleDetailsActivity;
import com.example.myhealth.myhealth.screen.NewsDetails.NewsDetailsActivity;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/6/2018.
 */

public class NewsFragment extends Fragment implements NewsView, NewsAdapter.ItemClickListener {

    private RecyclerView recyclerView;
    private NewsAdapter newsAdapter;
    private NewsPresenter newsPresenter;
    private int page = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static NewsFragment newInstance() {
        return new NewsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_fragment, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.news);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        Bundle bundle = getArguments();

        recyclerView.setOnScrollListener(new NewsAdapter.EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page  = current_page;
                getNews();
            }
        });
        newsAdapter = new NewsAdapter(getActivity());
        newsPresenter = new NewsPresenter(this);
        recyclerView.setAdapter(newsAdapter);
        getNews();
        return rootView;
    }

    private void getNews()
    {
        newsPresenter.getNews(page);
    }

    @Override
    public void setNews(List<News> news) {
        newsAdapter.setNews(news);
        newsAdapter.setClickListener(this);
    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);
        intent.putExtra("news", new Gson().toJson(newsAdapter.getItem(position)));
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void OnShareClick(View view, int position) {
        Intent share = new Intent(Intent.ACTION_SEND);

        share.putExtra(android.content.Intent.EXTRA_TEXT, newsAdapter.getItem(position).getContent() + " From MyHealth");
        share.setType("text/plain");

//
        startActivity(Intent.createChooser(share, "MyHealth"));
    }
}
