package com.example.myhealth.myhealth.screen.SignUp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.SignIn.SignInActivity;
import com.example.myhealth.myhealth.screen.SignIn.SignInFragment;
import com.example.myhealth.myhealth.screen.home.HomeActivity;
import com.google.gson.Gson;

/**
 * Created by Mayada Fahim on 4/29/2018.
 */

public class SignUpActivity extends BaseActivity {
    private TextView title;
    private ImageView back;
    private ImageView search;
    private View backView;
    private View searchView;
    private int type;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_activity);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        title = (TextView) findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        backView = (View) findViewById(R.id.backview);
        searchView = (View) findViewById(R.id.searchView);

        Bundle extras = getIntent().getExtras();
        type = extras.getInt("type");
        String file = extras.getString("file");
        String jsonMyObject;
        jsonMyObject = extras.getString("user");
        if(jsonMyObject != null) {
            user = new Gson().fromJson(jsonMyObject, User.class);
        }else {
            user = new User();
        }
        SignUpFragment signUpFragment = SignUpFragment.newInstance(type,new Gson().toJson(user), file);
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.signupActivity, signUpFragment, SignUpFragment.class.getSimpleName())
                .commit();
        search.setVisibility(View.GONE);
        title.setText("Sign Up");
        searchView.setVisibility(View.GONE);
        setBackClickListener();
    }

    private void  setBackClickListener()
    {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, SignInActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
    }
}
