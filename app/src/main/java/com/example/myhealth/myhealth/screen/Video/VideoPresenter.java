package com.example.myhealth.myhealth.screen.Video;

import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Video;
import com.example.myhealth.myhealth.api.responses.ListArticlesResponse;
import com.example.myhealth.myhealth.api.responses.ListVideosResponse;
import com.example.myhealth.myhealth.api.services.ArticleService;
import com.example.myhealth.myhealth.api.services.VideoService;
import com.example.myhealth.myhealth.screen.Article.ArticleView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 5/1/2018.
 */

public class VideoPresenter {

    private VideoView mView;

    public VideoPresenter(VideoView videoView) {
        mView = videoView;
    }

    public void getVideos(Integer page, Integer categoryId) {
        VideoService service = Network.getInstance().createService(VideoService.class);
        Log.e("here4", "here4");
        service.getArticles(categoryId, page).enqueue(new Callback<ListVideosResponse>() {
            @Override
            public void onResponse(Call<ListVideosResponse> call, Response<ListVideosResponse> response) {
                Log.e("here5", "here5");
                ListVideosResponse responseBody = response.body();
                List<Video> videos = new ArrayList<Video>();
                Log.e("here2", "here3");
                if (responseBody != null) {
                    videos = responseBody.getVideos();}
                mView.setVideos(videos);

            }

            @Override
            public void onFailure(Call<ListVideosResponse> call, Throwable t) {
                Log.e("here6", t.getMessage());
            }
        });
    }
}
