package com.example.myhealth.myhealth.screen.Article;

import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.responses.ListArticlesResponse;
import com.example.myhealth.myhealth.api.responses.ListDiseasesResponse;
import com.example.myhealth.myhealth.api.services.ArticleService;
import com.example.myhealth.myhealth.api.services.DiseaseService;
import com.example.myhealth.myhealth.screen.AToZ.AToZView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class ArticlePresenter {

    private ArticleView mView;

    public ArticlePresenter(ArticleView articleView) {
        mView = articleView;
    }

    public void getArticles(Integer page, Integer diseaseId) {
        ArticleService service = Network.getInstance().createService(ArticleService.class);
        service.getArticles(diseaseId, page).enqueue(new Callback<ListArticlesResponse>() {
            @Override
            public void onResponse(Call<ListArticlesResponse> call, Response<ListArticlesResponse> response) {
                ListArticlesResponse responseBody = response.body();
                List<Article> articles = new ArrayList<Article>();
                if (responseBody != null) {
                    articles = responseBody.getArticles();
                    Log.e("here2", "here3");
                    Log.e("here", articles.toString());
                }
                mView.setArticles(articles);

            }

            @Override
            public void onFailure(Call<ListArticlesResponse> call, Throwable t) {
                Log.e("here6", t.getMessage());
            }
        });
    }
}
