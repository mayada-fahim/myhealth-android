package com.example.myhealth.myhealth.screen.AToZ;

import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.responses.ListDiseasesResponse;
import com.example.myhealth.myhealth.api.services.DiseaseService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 4/21/2018.
 */

public class AToZPresenter {

    private AToZView mView;

    public AToZPresenter(AToZView aToZView) {
        mView = aToZView;
    }

    public void getDiseases(Integer page, String text) {
        DiseaseService service = Network.getInstance().createService(DiseaseService.class);

        service.getDiseases(page, text).enqueue(new Callback<ListDiseasesResponse>() {
            @Override
            public void onResponse(Call<ListDiseasesResponse> call, Response<ListDiseasesResponse> response) {
                ListDiseasesResponse responseBody = response.body();
                List<Disease> diseases = new ArrayList<Disease>();
                if (responseBody != null) {
                    diseases = responseBody.getDiseases();
                    Log.e("here", diseases.toString());
                }
                mView.setDiseases(diseases);

            }

            @Override
            public void onFailure(Call<ListDiseasesResponse> call, Throwable t) {

            }
        });
    }
}
