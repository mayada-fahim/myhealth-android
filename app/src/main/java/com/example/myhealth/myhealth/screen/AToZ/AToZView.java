package com.example.myhealth.myhealth.screen.AToZ;

import com.example.myhealth.myhealth.api.models.Disease;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/21/2018.
 */

public interface AToZView {
    public void setDiseases(List<Disease> diseases);
    public void error(String message);
    public void success();
}
