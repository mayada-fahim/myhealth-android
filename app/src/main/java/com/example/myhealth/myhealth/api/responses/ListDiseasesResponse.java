package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.Disease;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/21/2018.
 */

public class ListDiseasesResponse {


    @SerializedName("Results")
    @Expose
    public List<Disease> diseases = null;

    public List<Disease> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<Disease> diseases) {
        this.diseases = diseases;
    }
}
