package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.models.Speciality;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/30/2018.
 */

public class SpecialityListResponse {

    @SerializedName("Results")
    @Expose
    public List<Speciality> specialities = null;

    public List<Speciality> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(List<Speciality> specialities) {
        this.specialities = specialities;
    }
}
