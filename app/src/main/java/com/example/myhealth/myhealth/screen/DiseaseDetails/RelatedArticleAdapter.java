package com.example.myhealth.myhealth.screen.DiseaseDetails;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.screen.AToZ.DiseaseAdapter;
import com.example.myhealth.myhealth.screen.Constants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class RelatedArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Article> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    private Context context;

    public RelatedArticleAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        // this.mData = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.article_horizontal_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bindViewOnItem(position);

    }

    public void setArticles(List<Article> articles) {
        if (mData == null) {
            mData = articles;
        } else {
            // mData.clear();
            mData.addAll(articles);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public void clear() {
        if (mData == null) return;
        int size = this.mData.size();
        mData.clear();
        // notifyDataSetChanged();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private ImageView image;
        private TextView date;


        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.article_name);
            image = (ImageView) itemView.findViewById(R.id.article_image);
            date = (TextView) itemView.findViewById(R.id.article_date);
            itemView.setOnClickListener(this);


        }

        void bindViewOnItem(final int position) {
            Article article = mData.get(position);
            title.setText(article.getTitle());
            if(article.getImages().size() != 0) {
                Picasso.with(context).load(Constants.BASE_URL + article.getImages().get(0).getPath()).fit().centerCrop()
                        .into(image);
            }
        }

        @Override
        public void onClick(View view) {

            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


    public Article getItem(int position) {
        return mData.get(position);
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


}
