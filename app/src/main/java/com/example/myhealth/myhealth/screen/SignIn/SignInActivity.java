package com.example.myhealth.myhealth.screen.SignIn;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.home.HomeFragment;

/**
 * Created by Mayada Fahim on 4/28/2018.
 */

public class SignInActivity extends BaseActivity {

    private TextView title;
    private ImageView back;
    private ImageView search;
    private View backView;
    private View searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_activity);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        title = (TextView) findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        backView = (View) findViewById(R.id.backview);
        searchView = (View) findViewById(R.id.searchView);
        SignInFragment signInFragment = new SignInFragment();
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.signInActivitz, signInFragment, SignInFragment.class.getSimpleName())
                .commit();
        search.setVisibility(View.GONE);
        back.setVisibility(View.GONE);
        title.setText("Sign In");
        backView.setVisibility(View.GONE);
        searchView.setVisibility(View.GONE);
    }
}
