package com.example.myhealth.myhealth.screen.AToZ;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.home.HomeActivity;

/**
 * Created by Mayada Fahim on 4/21/2018.
 */

public class AToZActivity extends BaseActivity {

    private TextView title;
    private ImageView back;
    private ImageView search;
    private RelativeLayout mainLayout;
    private RelativeLayout searchLayout;
    private ImageView searchBack;
    private EditText searchText;
    private TextView cancel;
    private AToZFragment aToZFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_to_z_activity);
        title = (TextView) findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        cancel = (TextView) findViewById(R.id.cancel);
        mainLayout = (RelativeLayout) findViewById(R.id.default_layout);
        searchLayout = (RelativeLayout) findViewById(R.id.search_layout);
        searchBack = (ImageView) findViewById(R.id.back_green);
        searchText = (EditText) findViewById(R.id.search_text);
        aToZFragment = new AToZFragment();
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.aToZActivity, aToZFragment, AToZFragment.class.getSimpleName())
                .commit();

        title.setText("A To Z");
        setBackClickListener();
        searchClickListener();
    }

    private void searchClickListener() {
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchLayout.setVisibility(View.VISIBLE);
                mainLayout.setVisibility(View.GONE);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchLayout.setVisibility(View.GONE);
                mainLayout.setVisibility(View.VISIBLE);
                aToZFragment.clearData();
                aToZFragment.getDiseases("", 1);
                searchText.setText("");
            }
        });

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    aToZFragment.clearData();
                    aToZFragment.getDiseases(searchText.getText().toString(), 1);
                    hideKeyboardIfShown();
                    return true;
                }
                return false;
            }
        });

    }

    private void setBackClickListener() {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, HomeActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
        searchBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, HomeActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);

        this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
