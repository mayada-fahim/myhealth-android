package com.example.myhealth.myhealth.screen.SignUp;

import com.example.myhealth.myhealth.api.models.Speciality;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/30/2018.
 */

public interface SignUpView {
    public void error(String message);
    public void success();
    public void setSpecialities(List<Speciality> specialities);
}
