package com.example.myhealth.myhealth.screen.Article;

import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Disease;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public interface ArticleView {
    public void setArticles(List<Article> articles);
    public void error(String message);
    public void success();
}
