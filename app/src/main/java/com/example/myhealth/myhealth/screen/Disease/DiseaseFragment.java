package com.example.myhealth.myhealth.screen.Disease;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.screen.AToZ.AToZFragment;
import com.example.myhealth.myhealth.screen.AToZ.AToZPresenter;
import com.example.myhealth.myhealth.screen.AToZ.DiseaseAdapter;
import com.example.myhealth.myhealth.screen.Article.ArticleActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class DiseaseFragment extends Fragment implements DiseaseChildAdapter.ItemClickListener {

    private RecyclerView recyclerView;
    private DiseaseChildAdapter diseaseAdapter;
    private List<Disease> diseases;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static DiseaseFragment newInstance(String diseases) {
        DiseaseFragment diseaseFragment = new  DiseaseFragment();
        Bundle bundle = new Bundle();
        bundle.putString("diseases", diseases);
        diseaseFragment.setArguments(bundle);

        return diseaseFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.disease_fragment, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.diseases);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        String diseaseStr = getArguments().getString("diseases");
        Type listType = new TypeToken<ArrayList<Disease>>() {}.getType();
        diseases = new Gson().fromJson(diseaseStr, listType);
        diseaseAdapter = new DiseaseChildAdapter(getActivity(), diseases);
        recyclerView.setAdapter(diseaseAdapter);
        diseaseAdapter.setClickListener(this);
        return rootView;
    }

    @Override
    public void onItemClick(View view, int position) {
        Log.e("here","here");
        Intent intent = new Intent(getActivity(), ArticleActivity.class);
        intent.putExtra("disease", new Gson().toJson(diseaseAdapter.getItem(position)));
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}
