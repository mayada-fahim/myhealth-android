package com.example.myhealth.myhealth.api.services;

import com.example.myhealth.myhealth.api.responses.ListDoctorsResponse;
import com.example.myhealth.myhealth.api.responses.SignInResponse;
import com.example.myhealth.myhealth.api.responses.SignUpResponse;
import com.example.myhealth.myhealth.api.responses.SpecialityListResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Mayada Fahim on 4/30/2018.
 */

public interface UserService {

    @FormUrlEncoded
    @POST("users")
    Call<SignUpResponse> signUp(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("mobile_number") String mobileNumber,
            @Field("gender") int gender,
            @Field("image") String image,
            @Field("speciality_id") int specialityId,
            @Field("type_id") int userTypeId,
            @Field("id_number") String numberId);

    @FormUrlEncoded
    @POST("authenticate")
    Call<SignInResponse> signIn(
            @Field("email") String email,
            @Field("password") String password);


    @GET("users/currentUser")
    Call<SignInResponse> getCurrentUser();

    @GET("doctors/{page}")
    Call<ListDoctorsResponse> getDoctors(@Path("page") int page, @Query("search") String search);

    @FormUrlEncoded
    @PUT("users")
    Call<SignInResponse> update(
            @Field("name") String name,
            @Field("mobile_number") String mobileNumber,
            @Field("gender") int gender,
            @Field("image") String image,
            @Field("speciality_id") int specialityId,
            @Field("job_id") int jobId,
            @Field("profile_image") String profileImage,
            @Field("country") String country,
            @Field("birth_date") String birthDate,
            @Field("email") String email,
            @Field("password") String password,
            @Field("bio") String bio);

}
