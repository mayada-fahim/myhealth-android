package com.example.myhealth.myhealth.screen.SignUp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.Constants;
import com.example.myhealth.myhealth.screen.SignIn.SignInActivity;
import com.google.gson.Gson;

/**
 * Created by Mayada Fahim on 4/29/2018.
 */

public class TypeFragment extends Fragment {

    private ImageView user;
    private ImageView userSelected;
    private ImageView doctor;
    private ImageView doctorSelected;
    private Integer type;
    private Button next;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.type_fragment, container, false);
        user = (ImageView) rootView.findViewById(R.id.user_white);
        userSelected = (ImageView) rootView.findViewById(R.id.user_selected);
        doctor = (ImageView) rootView.findViewById(R.id.doctor_white);
        doctorSelected = (ImageView) rootView.findViewById(R.id.doctor_selected);
        next = (Button) rootView.findViewById(R.id.next_button);
        setUserSelectClickListener();
        setDoctorSelectClickListener();
        setNextClickListener();
        return rootView;
    }

    private void setUserSelectClickListener() {
        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userSelected.setVisibility(View.VISIBLE);
                doctorSelected.setVisibility(View.GONE);
                type = 0;
            }
        });
    }

    private void setDoctorSelectClickListener() {
        doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userSelected.setVisibility(View.GONE);
                doctorSelected.setVisibility(View.VISIBLE);
                type = 1;
            }
        });
    }

    private void setNextClickListener() {
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type != null && type == 0) {
                    Intent intent = new Intent(getActivity(), SignUpActivity.class);
                    intent.putExtra("type", Constants.USER);
                    startActivity(intent);

                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                } else if (type != null && type == 1) {
                    Intent intent = new Intent(getActivity(), DoctorIdActivity.class);
                    startActivity(intent);

                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });
    }
}
