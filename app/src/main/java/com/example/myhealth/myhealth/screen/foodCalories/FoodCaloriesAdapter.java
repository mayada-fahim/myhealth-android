package com.example.myhealth.myhealth.screen.foodCalories;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.DummyModelCalories;
import com.example.myhealth.myhealth.screen.News.NewsAdapter;

import java.util.List;


/**
 * Created by Hossam Elsawy on 28/05/18.
 */
public class FoodCaloriesAdapter extends RecyclerView.Adapter<FoodCaloriesAdapter.FoodCaloriesViewHolder> {


    private LayoutInflater mInflater;
    private FoodCaloriesAdapter.ItemClickListener mClickListener;
    List<DummyModelCalories> modelCalories;

    private Context context;

    public FoodCaloriesAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);


    }

    @NonNull
    @Override
    public FoodCaloriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.calories_item, parent, false);
        return new FoodCaloriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodCaloriesViewHolder holder, int position) {

        holder.bindViewOnItem(position);

    }

    @Override
    public int getItemCount() {
        return modelCalories != null ? modelCalories.size() : 0;
    }

    public void setModelCalories(List<DummyModelCalories> dummyModelCalories) {
        if (modelCalories == null) {
            modelCalories = dummyModelCalories;
        } else {
            // mData.clear();
            modelCalories.addAll(dummyModelCalories);
        }
        notifyDataSetChanged();
    }

    public abstract static class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
        public String TAG = NewsAdapter.EndlessRecyclerOnScrollListener.class.getSimpleName();
        int firstVisibleItem, visibleItemCount, totalItemCount;
        private int previousTotal = 0; // The total number of items in the dataset after the last load
        private boolean loading = true; // True if we are still waiting for the last set of data to load.
        private int visibleThreshold = 5; // The minimum amount of items to have below your current scroll position before loading more.
        private int current_page = 1;

        private LinearLayoutManager mLinearLayoutManager;

        public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
            this.mLinearLayoutManager = linearLayoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            visibleItemCount = recyclerView.getChildCount();
            totalItemCount = mLinearLayoutManager.getItemCount();
            firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)) {
                // End has been reached

                // Do something
                current_page++;

                onLoadMore(current_page);

                loading = true;
            }
        }

        public abstract void onLoadMore(int current_page);
    }

    // allows clicks events to be caught
    public void setClickListener(FoodCaloriesAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public DummyModelCalories getItem(int position) {


        return modelCalories.get(position);
    }

    public class FoodCaloriesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private ImageView image;


        public FoodCaloriesViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.calories_name);
            image = itemView.findViewById(R.id.calories_image);
            itemView.setOnClickListener(this);


        }

        void bindViewOnItem(final int position) {
            DummyModelCalories dummyModelCalories = modelCalories.get(position);

            if (dummyModelCalories != null) {
                title.setText(dummyModelCalories.getTitle());
//                Picasso.with(context).load(R.drawable.c_l_bakery).fit().centerCrop()
//                        .into(image);

                image.setImageResource(dummyModelCalories.getImage());
            }
        }

        @Override
        public void onClick(View view) {

            if (mClickListener != null) mClickListener.onItemClick(view, getLayoutPosition());

        }
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
