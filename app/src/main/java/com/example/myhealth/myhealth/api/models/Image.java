package com.example.myhealth.myhealth.api.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class Image {

    @SerializedName("path")
    public String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
