package com.example.myhealth.myhealth.screen.foodCalories;

import com.example.myhealth.myhealth.api.models.DummyModelDetailsCalories;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hossam Elsawy on 02/06/18.
 */
public class CaloriesDetialsPresenter {

    CaloriesDetailsView mView;
    List<DummyModelDetailsCalories> dummyModelCalories = new ArrayList<>();


    public CaloriesDetialsPresenter(CaloriesDetailsView caloriesDetailsView) {
        this.mView = caloriesDetailsView;

    }

    public void getCaloriesDeatilsModel() {


        DummyModelDetailsCalories dummyModelCalories1 = new DummyModelDetailsCalories();

        dummyModelCalories1.setName("Bacardi");
        dummyModelCalories1.setDetails("1 single/275ml-52cl");

        DummyModelDetailsCalories dummyModelCalories2 = new DummyModelDetailsCalories();

        dummyModelCalories2.setName("Baileys Irish cream");
        dummyModelCalories2.setDetails("1 Glass/37ml-130cl");
        DummyModelDetailsCalories dummyModelCalories3 = new DummyModelDetailsCalories();

        dummyModelCalories3.setName("Bacardi");
        dummyModelCalories3.setDetails("1 single/275ml-52cl");

        dummyModelCalories.add(dummyModelCalories1);
        dummyModelCalories.add(dummyModelCalories2);
        dummyModelCalories.add(dummyModelCalories3);


        mView.setCaloriesDetials(dummyModelCalories);


    }
}