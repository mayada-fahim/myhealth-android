package com.example.myhealth.myhealth.screen.Clinic;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Category;
import com.example.myhealth.myhealth.screen.AToZ.AToZFragment;
import com.example.myhealth.myhealth.screen.AToZ.AToZPresenter;
import com.example.myhealth.myhealth.screen.AToZ.DiseaseAdapter;
import com.example.myhealth.myhealth.screen.Article.ArticleActivity;
import com.example.myhealth.myhealth.screen.Video.VideoActivity;
import com.google.gson.Gson;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class ClinicFragment extends android.support.v4.app.Fragment implements CategoryAdapter.ItemClickListener, ClinicView {

    private RecyclerView recyclerView;
    private CategoryAdapter categoryAdapter;
    private ClinicPresenter clinicPresenter;
    private int page = 1;
    private  String text = "";
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ClinicFragment newInstance() {
        return new ClinicFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.clinic_fragment, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.categories);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setOnScrollListener(new DiseaseAdapter.EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page = current_page;
                getCategories(text, page);
            }
        });
        categoryAdapter = new CategoryAdapter(getActivity());
        clinicPresenter = new ClinicPresenter(this);
        recyclerView.setAdapter(categoryAdapter);
        getCategories("", page);
        return rootView;
    }

    public void getCategories(String text, int page) {
        progressBar.setVisibility(View.VISIBLE);
        this.page = page;
        clinicPresenter.getCategories(page, text);
    }

    public void clearData() {
        categoryAdapter.clearData();
    }

    @Override
    public void setCategories(List<Category> categories) {
        progressBar.setVisibility(View.GONE);
        categoryAdapter.setDiseases(categories);
        categoryAdapter.setClickListener(this);
    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), VideoActivity.class);
        intent.putExtra("category", new Gson().toJson(categoryAdapter.getItem(position)));
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}
