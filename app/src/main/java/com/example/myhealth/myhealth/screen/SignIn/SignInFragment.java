package com.example.myhealth.myhealth.screen.SignIn;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myhealth.myhealth.BuildConfig;
import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.AToZ.AToZActivity;
import com.example.myhealth.myhealth.screen.SignUp.TypeActivity;
import com.example.myhealth.myhealth.screen.UserHelper;
import com.example.myhealth.myhealth.screen.home.HomeActivity;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Mayada Fahim on 4/28/2018.
 */

public class SignInFragment extends Fragment implements SignInView {
    private CallbackManager callbackManager;
    private TextView signUp;
    private EditText email;
    private TextView emailError;
    private EditText password;
    private TextView passwordError;
//    private ProgressBar progressBar;
    private ProgressDialog dialog;
    private Button signIn;
    private boolean error;
    private SignInPresenter signInPresenter;
    private SharedPreferences.Editor editor;
    private SharedPreferences prefs;
    private TextView skip;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sign_in_fragment, container, false);
        signUp = (TextView) rootView.findViewById(R.id.sign_up);
        email = (EditText) rootView.findViewById(R.id.email);
        emailError = (TextView) rootView.findViewById(R.id.email_error);
        passwordError = (TextView) rootView.findViewById(R.id.password_error);
        password = (EditText) rootView.findViewById(R.id.password);
        password.setTypeface(Typeface.DEFAULT);
        password.setTransformationMethod(new PasswordTransformationMethod());
        dialog = new ProgressDialog(getActivity());
        //progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        signIn = (Button) rootView.findViewById(R.id.signin_button);
        skip = (TextView) rootView.findViewById(R.id.skip_login);
        signInPresenter = new SignInPresenter(this);
        prefs = getActivity().getSharedPreferences("myHealth", MODE_PRIVATE);
        setSignUpClickListener();
        setSignInClickListener();
        setSkipClickListener();
        return rootView;
    }

    private void setSkipClickListener() {
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    private void setSignUpClickListener() {
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), TypeActivity.class);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    private void setSignInClickListener() {
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordError.setVisibility(View.GONE);
                emailError.setVisibility(View.GONE);
                error = false;
                if (email.getText().toString().replaceAll("\\s+", "").matches("")) {
                    error = true;
                    emailError.setVisibility(View.VISIBLE);
                    emailError.setText("Please enter you email");
                }

                if (password.getText().toString().matches("")) {
                    error = true;
                    passwordError.setVisibility(View.VISIBLE);
                    passwordError.setText("Please enter you password");
                }
                if (!email.getText().toString().replaceAll("\\s+", "").matches(emailPattern)) {
                    error = true;
                    emailError.setText("Please enter correct email format");
                    emailError.setVisibility(View.VISIBLE);
                }
                if (!error) {
                    //progressBar.setVisibility(View.VISIBLE);
                    dialog.setMessage("Loading");
                    dialog.show();
                   // signIn.setVisibility(View.GONE);
                    signInPresenter.signIn(email.getText().toString(), password.getText().toString());
                }
            }
        });

    }

    @Override
    public void error(String message) {
        passwordError.setVisibility(View.VISIBLE);
        passwordError.setText(message);
        //progressBar.setVisibility(View.GONE);
        dialog.dismiss();
        signIn.setVisibility(View.VISIBLE);

    }

    @Override
    public void success() {

    }

    @Override
    public void setUser(User user) {
        UserHelper userHelper = new UserHelper(getContext());
        signInPresenter.sendToken(userHelper.getDeviceToken(), user.getId() + "");
        editor = getActivity().getSharedPreferences("myHealth", MODE_PRIVATE).edit();
        editor.putString("email", user.getEmail());
        editor.putInt("type", user.getTypeId());
        editor.putString("token", user.getToken());
        editor.putString("name", user.getName());
        editor.putInt("id", user.getId());
        if (user.getProfileImage() != null) {
            editor.putString("image", user.getProfileImage());
        }
        editor.apply();
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}
