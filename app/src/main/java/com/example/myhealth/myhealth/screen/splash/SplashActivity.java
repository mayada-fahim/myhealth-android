package com.example.myhealth.myhealth.screen.splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.SignIn.SignInActivity;
import com.example.myhealth.myhealth.screen.home.HomeActivity;

/**
 * Created by Mayada Fahim on 4/28/2018.
 */

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                SharedPreferences prefs = getSharedPreferences("myHealth", MODE_PRIVATE);
                if (prefs.getString("name", null) != null) {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);

                    SplashActivity.this.overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(SplashActivity.this, SignInActivity.class);
                    startActivity(intent);

                    SplashActivity.this.overridePendingTransition(R.anim.enter, R.anim.exit);
                }

                // close this activity
                finish();
            }
        }, 2000);

    }
}
