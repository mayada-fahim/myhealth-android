package com.example.myhealth.myhealth.screen.DiseaseDetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.models.Symptom;
import com.example.myhealth.myhealth.screen.ArticleDetails.ArticleDetailsActivity;
import com.example.myhealth.myhealth.screen.Disease.DiseaseChildAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */
import java.util.ArrayList;
import java.util.List;

public class DiseaseDetailsFragment extends Fragment implements DiseaseDetailsView, RelatedArticleAdapter.ItemClickListener {

    private RecyclerView recyclerView;
    private List<Symptom> symptoms;
    private TextView name;
    private TextView description;
    private DiseaseDetailsPresenter diseaseDetailsPresenter;
    private SymptomAdapter symptomAdapter;
    private RelatedArticleAdapter relatedArticleAdapter;
    private RecyclerView articleRecylerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.disease_details_fragment, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.sypmtom);
        articleRecylerView = (RecyclerView) rootView.findViewById(R.id.articles);
        name = (TextView) rootView.findViewById(R.id.name);
        description = (TextView) rootView.findViewById(R.id.description);
        symptomAdapter = new SymptomAdapter(getActivity());
        relatedArticleAdapter = new RelatedArticleAdapter(getActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        articleRecylerView.setLayoutManager(gridLayoutManager);

        articleRecylerView.setAdapter(relatedArticleAdapter);

        recyclerView.setAdapter(symptomAdapter);
        int id = getArguments().getInt("id");
        diseaseDetailsPresenter = new DiseaseDetailsPresenter(this);
        Log.e("here", id+"");
        getDisease(id);
        return rootView;
    }

    public void getDisease(int id)
    {
        diseaseDetailsPresenter.getDisease(id);
    }

    @Override
    public void setDisease(Disease disease) {
        name.setText(disease.getName());
        description.setText(disease.getDescription());
        Log.e("here", disease.getSymptoms().size() + "");
        symptomAdapter.setSymptoms(disease.getSymptoms());
    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), ArticleDetailsActivity.class);
        intent.putExtra("article", new Gson().toJson(relatedArticleAdapter.getItem(position)));
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void setArticles(List<Article> articles) {
        relatedArticleAdapter.setArticles(articles);
        relatedArticleAdapter.setClickListener(this);
    }
}
