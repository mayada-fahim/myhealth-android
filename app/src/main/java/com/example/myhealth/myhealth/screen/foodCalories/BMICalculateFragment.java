package com.example.myhealth.myhealth.screen.foodCalories;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.BMIRepresent;


public class BMICalculateFragment extends Fragment implements BMICalculateView {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;
    private TextView headertext;
    private EditText height;
    private EditText weight;
    private TextView heightError;
    private TextView weightError;
    private Button calculatebmi;
    private BMICalculatePresenter bmiCalculatePresenter;


    public BMICalculateFragment() {
        // Required empty public constructor
    }


    public static BMICalculateFragment newInstance(String param1, String param2) {
        BMICalculateFragment fragment = new BMICalculateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_bmicalculate, container, false);
        calculatebmi = view.findViewById(R.id.calculate_bmi);
        weight = view.findViewById(R.id.weight);
        height = view.findViewById(R.id.height);
        headertext = view.findViewById(R.id.header_text);
        heightError = view.findViewById(R.id.height_error);
        weightError = view.findViewById(R.id.weight_error);
        bmiCalculatePresenter = new BMICalculatePresenter(this, getActivity().getApplicationContext());
        calculatebmi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                heightError.setVisibility(View.GONE);
                weightError.setVisibility(View.GONE);


                if (bmiCalculatePresenter.validateInputParam(weight, height))
                    bmiCalculatePresenter.calculateBmiResult(Double.valueOf(height.getText().toString()), Double.valueOf(weight.getText().toString()));

            }
        });
        return view;

    }

    @Override
    public void getBMIResult(BMIRepresent bmiRepresent) {
        Intent intent = new Intent(getActivity(), BMIResultActivity.class);
        intent.putExtra(BMIResultActivity.BMI_REPRESENT_MODEL, bmiRepresent);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);


    }

    @Override
    public void setErrorHeight() {

        heightError.setVisibility(View.VISIBLE);

    }

    @Override
    public void setErrorWeight() {

        weightError.setVisibility(View.VISIBLE);

    }


}
