package com.example.myhealth.myhealth.screen.foodCalories;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.DummyModelCalories;
import com.example.myhealth.myhealth.screen.News.NewsAdapter;

import java.util.List;

import static com.example.myhealth.myhealth.screen.foodCalories.CaloriesDetailsActivity.CALORIES_LIST_ID;


public class CaloriesListFragment extends Fragment implements CaloriesListView, FoodCaloriesAdapter.ItemClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;


    private RecyclerView recyclerView;
    private FoodCaloriesAdapter foodCaloriesAdapter;
    private CaloriesListPresenter caloriesListPresenter;
    private int page = 1;
    private ProgressBar progressBar;



    public CaloriesListFragment() {
        // Required empty public constructor
    }


    public static CaloriesListFragment newInstance(String param1, String param2) {
        CaloriesListFragment fragment = new CaloriesListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_calories_list, container, false);

        recyclerView = view.findViewById(R.id.calories_rv);
        progressBar = view.findViewById(R.id.progressBar);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new NewsAdapter.EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page = current_page;
                getCaloriesList();
            }
        });


        foodCaloriesAdapter = new FoodCaloriesAdapter(getContext());
        caloriesListPresenter = new CaloriesListPresenter(this);
        recyclerView.setAdapter(foodCaloriesAdapter);

        getCaloriesList();

        return view;


    }


    private void getCaloriesList() {

        progressBar.setVisibility(View.VISIBLE);

        caloriesListPresenter.getCaloriesModel();


    }

    @Override
    public void setCalories(List<DummyModelCalories> dummyModelCalories) {


        progressBar.setVisibility(View.GONE);
        foodCaloriesAdapter.setModelCalories(dummyModelCalories);
        foodCaloriesAdapter.setClickListener(this);

    }

    @Override
    public void onItemClick(View view, int position) {

        DummyModelCalories item = foodCaloriesAdapter.getItem(position);


        Intent intent = new Intent(getActivity(), CaloriesDetailsActivity.class);
        intent.putExtra(CALORIES_LIST_ID, item);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);


    }
}
