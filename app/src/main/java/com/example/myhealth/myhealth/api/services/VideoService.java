package com.example.myhealth.myhealth.api.services;

import com.example.myhealth.myhealth.api.responses.ListArticlesResponse;
import com.example.myhealth.myhealth.api.responses.ListVideosResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Mayada Fahim on 5/1/2018.
 */

public interface VideoService {

    @GET("videos/{category_id}/{page}")
    Call<ListVideosResponse> getArticles(@Path("category_id") int categoryId, @Path("page") int page);
}
