package com.example.myhealth.myhealth.api.services;

import com.example.myhealth.myhealth.api.responses.ListCategoriesResponse;
import com.example.myhealth.myhealth.api.responses.ListDiseasesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public interface CategoryService {

    @GET("categories/{page}")
    Call<ListCategoriesResponse> getCategories(@Path("page") int page, @Query("search") String search);


}