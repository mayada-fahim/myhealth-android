package com.example.myhealth.myhealth.screen.foodCalories;

import com.example.myhealth.myhealth.api.models.DummyModelCalories;

import java.util.List;

/**
 * Created by Hossam Elsawy on 28/05/18.
 */
public interface CaloriesListView {

    void setCalories(List<DummyModelCalories> dummyModelCalories);
}
