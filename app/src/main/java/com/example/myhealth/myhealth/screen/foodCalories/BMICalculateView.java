package com.example.myhealth.myhealth.screen.foodCalories;

import com.example.myhealth.myhealth.api.models.BMIRepresent;

/**
 * Created by Hossam Elsawy on 28/05/18.
 */
public interface BMICalculateView {


    void getBMIResult(BMIRepresent bmiRepresent);

    void setErrorHeight();

    void setErrorWeight();


}
