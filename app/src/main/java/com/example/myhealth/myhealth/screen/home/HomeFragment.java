package com.example.myhealth.myhealth.screen.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.AToZ.AToZActivity;
import com.example.myhealth.myhealth.screen.Clinic.ClinicActivity;
import com.example.myhealth.myhealth.screen.Doctor.DoctorActivity;
import com.example.myhealth.myhealth.screen.News.NewsActivity;
import com.example.myhealth.myhealth.screen.Symptoms.SymptomsActivity;
import com.example.myhealth.myhealth.screen.foodCalories.FoodCaloriesActivity;

/**
 * Created by Mayada Fahim on 4/21/2018.
 */

public class HomeFragment extends Fragment {

    private RelativeLayout aToZ;
    private RelativeLayout clinic;
    private RelativeLayout symptoms;
    private RelativeLayout doctors;
    private RelativeLayout news;
    private RelativeLayout foodCalories;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.home_fragment, container, false);

        aToZ = (RelativeLayout) rootView.findViewById(R.id.aToZ);
        clinic = (RelativeLayout) rootView.findViewById(R.id.clinic);
        symptoms = (RelativeLayout) rootView.findViewById(R.id.symptoms);
        news = (RelativeLayout) rootView.findViewById(R.id.news);
        doctors = (RelativeLayout) rootView.findViewById(R.id.doctors);

        foodCalories = rootView.findViewById(R.id.food_calories);
        setCategoriesClickListener();
        return rootView;
    }

    public void setCategoriesClickListener() {
        aToZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AToZActivity.class);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        clinic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ClinicActivity.class);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        symptoms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SymptomsActivity.class);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NewsActivity.class);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        doctors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DoctorActivity.class);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        foodCalories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), FoodCaloriesActivity.class);
//
//                startActivity(intent);
//
//                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

}
