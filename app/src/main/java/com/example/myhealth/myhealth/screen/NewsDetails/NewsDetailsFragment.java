package com.example.myhealth.myhealth.screen.NewsDetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.News;
import com.example.myhealth.myhealth.screen.ArticleDetails.ArticleDetailsFragment;
import com.example.myhealth.myhealth.screen.Constants;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

/**
 * Created by Mayada Fahim on 5/6/2018.
 */

public class NewsDetailsFragment extends Fragment {
    private News news;
    private CarouselView carouselView;
    private TextView title;
    private TextView body;
    private ImageView like;
    private ImageView share;
    private TextView author;
    private TextView date;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static NewsDetailsFragment newInstance(String news) {
        NewsDetailsFragment newsDetailsFragment = new NewsDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("news", news);
        newsDetailsFragment.setArguments(bundle);

        return newsDetailsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_details_fragment, container, false);
        String userStr = getArguments().getString("news");
        news = new Gson().fromJson(userStr, News.class);
        carouselView = (CarouselView) rootView.findViewById(R.id.carouselView);
        title = rootView.findViewById(R.id.newsTitle);
        body = rootView.findViewById(R.id.news_body);
        author = rootView.findViewById(R.id.authorName);
        date = (TextView) rootView.findViewById(R.id.date);
        like = rootView.findViewById(R.id.like);
        share = rootView.findViewById(R.id.share);
        title.setText(news.getTitle());
        body.setText(news.getContent());
        date.setText(news.getDate().split(" ")[0]);
        if (news.getUser() != null) {
            author.setText(news.getUser().getName());
        } else {
            author.setText("Unknown");
        }
        carouselView.setPageCount(news.getImages().size());

        carouselView.setImageListener(imageListener);
        setShareClickListener();
        return rootView;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Picasso.with(getContext()).load(Constants.BASE_URL + news.getImages().get(position).getPath())
                    .into(imageView);
            //imageView.setImageResource(Constants.BASE_URL + article.getImages().get(position));
        }
    };

    public void setShareClickListener() {
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(Intent.ACTION_SEND);

                share.putExtra(android.content.Intent.EXTRA_TEXT, news.getContent() + " From MyHealth");
                share.setType("text/plain");

//
                startActivity(Intent.createChooser(share, "MyHealth"));
            }
        });
    }
}
