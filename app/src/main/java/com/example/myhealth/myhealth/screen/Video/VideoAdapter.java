package com.example.myhealth.myhealth.screen.Video;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.models.Video;
import com.example.myhealth.myhealth.screen.AToZ.DiseaseAdapter;
import com.example.myhealth.myhealth.screen.Constants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Video> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    private Context context;

    public VideoAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        // this.mData = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.video_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bindViewOnItem(position);

    }

    public void setVideos(List<Video> videos) {
        if (mData == null) {
            mData = videos;
        } else {
            // mData.clear();
            mData.addAll(videos);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public void clear() {
        if (mData == null) return;
        int size = this.mData.size();
        mData.clear();
        // notifyDataSetChanged();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private ImageView image;
        private TextView date;
        private TextView author;
        private ImageView share;
        private ImageView like;


        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.videoTitle);
            image = (ImageView) itemView.findViewById(R.id.videoImage);
            date = (TextView) itemView.findViewById(R.id.date);
            author = (TextView) itemView.findViewById(R.id.authorName);
            share = (ImageView) itemView.findViewById(R.id.share);
            like = (ImageView) itemView.findViewById(R.id.like);
            image.setOnClickListener(this);
            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.OnShareClick(view, getAdapterPosition());
                }
            });

        }

        void bindViewOnItem(final int position) {
            Video video = mData.get(position);
            title.setText(video.getTitle());
            if (video.getUser() != null) {
                author.setText(video.getUser().getName());
            } else {
                author.setText("Unknown");
            }
            date.setText(video.getDate().split(" ")[0]);
            Log.e("here", getYouTubeId(video.getUrl()));
            Picasso.with(context).load("https://img.youtube.com/vi/" + getYouTubeId(video.getUrl()) + "/0.jpg").fit().centerCrop()
                    .into(image);
        }

        @Override
        public void onClick(View view) {

            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }
    }

    private String getYouTubeId (String youTubeUrl) {
        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if(matcher.find()){
            return matcher.group();
        } else {
            return "error";
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


    public Video getItem(int position) {
        return mData.get(position);
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);

        void OnShareClick(View view, int position);
    }

    public abstract static class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
        public String TAG = VideoAdapter.EndlessRecyclerOnScrollListener.class.getSimpleName();

        private int previousTotal = 0; // The total number of items in the dataset after the last load
        private boolean loading = true; // True if we are still waiting for the last set of data to load.
        private int visibleThreshold = 5; // The minimum amount of items to have below your current scroll position before loading more.
        int firstVisibleItem, visibleItemCount, totalItemCount;

        private int current_page = 1;

        private LinearLayoutManager mLinearLayoutManager;

        public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
            this.mLinearLayoutManager = linearLayoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            visibleItemCount = recyclerView.getChildCount();
            totalItemCount = mLinearLayoutManager.getItemCount();
            firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)) {
                // End has been reached

                // Do something
                current_page++;

                onLoadMore(current_page);

                loading = true;
            }
        }

        public abstract void onLoadMore(int current_page);
    }

}
