package com.example.myhealth.myhealth.api;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.myhealth.myhealth.screen.UserHelper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Belal Mohamed on 7/2/2017.
 */

/**
 * Singleton class it is responsible for generating the services.
 */

public class Network {

    private static Network mNetwork;
    private static Retrofit mRetrofit;
    private UserHelper userHelper;
    private static final String BASE_URL = "https://myhealth.itjanus.com/healthdigest/BackEnd/public/api/v1/mobile/";

    //private methods.
    private Network() {
        mRetrofit = createRetrofit();

    }

    private Network(UserHelper userHelper) {
        mRetrofit = createRetrofit();
        this.userHelper = userHelper;

    }

    private HttpLoggingInterceptor createHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return loggingInterceptor;
    }

    private OkHttpClient createOkHttpClient(){
        return new OkHttpClient.Builder().connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(createHttpLoggingInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Accept", "application/json;versions=1");
                        if (userHelper != null && userHelper.isUserLoggedIn()) {
                            ongoing.addHeader("Authorization", "Bearer " + userHelper.getToken());
                        }
                        return chain.proceed(ongoing.build());
                    }
                })
                .build();
    }

    private Retrofit createRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(createOkHttpClient())
                .build();
    }

    //public methods.
    public static Network getInstance() {
        if (mNetwork == null) {
            mNetwork = new Network();
        }
        return mNetwork;
    }

    public static Network getInstanceWithHelper(UserHelper userHelper) {
        if (mNetwork == null) {
            mNetwork = new Network(userHelper);
        }
        return mNetwork;
    }

    public <T> T createService(Class<T> service) {
        return mRetrofit.create(service);
    }
}
