package com.example.myhealth.myhealth.api.services;

import com.example.myhealth.myhealth.api.responses.AddTokenResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Mayada Fahim on 6/17/2018.
 */

public interface TokenService {

    @FormUrlEncoded
    @POST("tokens/add")
    Call<AddTokenResponse> addToken(@Field("token") String token, @Field("user_id") String userId);
}
