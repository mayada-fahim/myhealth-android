package com.example.myhealth.myhealth.screen.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.CircleTransform;
import com.example.myhealth.myhealth.screen.Constants;
import com.example.myhealth.myhealth.screen.Profile.ProfileActivity;
import com.example.myhealth.myhealth.screen.SignIn.SignInActivity;
import com.example.myhealth.myhealth.screen.UserHelper;
import com.example.myhealth.myhealth.screen.splash.SplashActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by Mayada Fahim on 4/21/2018.
 */

public class HomeActivity extends BaseActivity {

    private FragmentTransaction mFragmentTransaction;
    private FragmentManager mFragmentManager;
    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
    private TextView userName;
    private ImageView userImage;
    private ImageView menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        menu = (ImageView) findViewById(R.id.menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        UserHelper userHelper = new UserHelper(this);
        if(userHelper.isUserLoggedIn()) {
            nav_Menu.findItem(R.id.signIn).setVisible(false);
        }else {
            nav_Menu.findItem(R.id.logout).setVisible(false);
        }
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        int id = menuItem.getItemId();

                        if (id == R.id.logout) {
                            logout();
                        }
                        if(id == R.id.signIn)
                        {
                            logout();
                        }
                        // set item as selected to persist highlight
                        //menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });
        HomeFragment homeFragment = new HomeFragment();
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.homeActivity, homeFragment, HomeFragment.class.getSimpleName())
                .commit();
        hideMenu();
        triggerMenu();
    }

    private void triggerMenu()
    {
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        });
    }

    private void hideMenu() {
        SharedPreferences prefs = getSharedPreferences("myHealth", MODE_PRIVATE);
        if (prefs.getString("name", null) == null) {
           // mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            View header = navigationView.getHeaderView(0);
            userName = (TextView) header.findViewById(R.id.name);
            userImage = (ImageView) header.findViewById(R.id.userProfile);
            userName.setText(prefs.getString("name", null));
            if (prefs.getString("image", null) != null) {
                Picasso.with(this).load(Constants.BASE_URL + prefs.getString("image", null)).transform(new CircleTransform())
                        .into(userImage);
            }
            userImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                    startActivity(intent);

                    HomeActivity.this.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            });
        }
    }

    private void logout() {
        SharedPreferences.Editor editor = getSharedPreferences("myHealth", MODE_PRIVATE).edit();
        editor.putString("email", null);
        editor.putInt("type", 0);
        editor.putString("token", null);
        editor.putString("name", null);
        editor.apply();
        Intent intent = new Intent(HomeActivity.this, SignInActivity.class);
        startActivity(intent);

        HomeActivity.this.overridePendingTransition(R.anim.enter, R.anim.exit);
    }


}
