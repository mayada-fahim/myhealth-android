package com.example.myhealth.myhealth.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayada Fahim on 5/20/2018.
 */

public class UploadImageResponse {

    @SerializedName("url")
    @Expose
    public String url;
}
