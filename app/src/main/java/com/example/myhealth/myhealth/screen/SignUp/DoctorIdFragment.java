package com.example.myhealth.myhealth.screen.SignUp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Speciality;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.Constants;
import com.example.myhealth.myhealth.utils.ImageUploader;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Mayada Fahim on 4/29/2018.
 */

public class DoctorIdFragment extends Fragment implements SignUpView {

    private ImageView captureId;
    private EditText id;
    private Spinner speciality;
    private Button next;
    private TextView idError;
    private TextView specialityError;
    private String selectedSpeciality;
    private User user;
    private SignUpPresenter signUpPresenter;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    private ImageView image;
    private String filePath;
    private TextView imageText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.doctor_id_fragment, container, false);
        next = (Button) rootView.findViewById(R.id.next_button);
        id = (EditText) rootView.findViewById(R.id.id_number);
        image = (ImageView) rootView.findViewById(R.id.image);
        imageText = (TextView) rootView.findViewById(R.id.image_text);
        speciality = (Spinner) rootView.findViewById(R.id.speciality_spinner);
        idError = (TextView) rootView.findViewById(R.id.id_error);
        specialityError = (TextView) rootView.findViewById(R.id.speciality_error);
        user = new User();
        signUpPresenter = new SignUpPresenter(this);
        setUpSpecialitySpinner();
        setNextClickListener();
        setImageClickListener();
        return rootView;
    }

    private void setImageClickListener()
    {
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestGalleryPermission();
            }
        });
    }

    private void setUpSpecialitySpinner() {
        signUpPresenter.getSpecialities();
    }

    private void setNextClickListener() {
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                idError.setVisibility(View.GONE);
                specialityError.setVisibility(View.GONE);
                if (id.getText().toString() == null || id.getText().toString().matches("")) {
                    idError.setVisibility(View.VISIBLE);
                }
                if (selectedSpeciality == null) {
                    specialityError.setVisibility(View.VISIBLE);
                }

                if (selectedSpeciality != null && !id.getText().toString().matches("")) {
                    user.setIdNumber(id.getText().toString());
                    Intent intent = new Intent(getActivity(), SignUpActivity.class);
                    intent.putExtra("type", Constants.DOCTOR);
                    intent.putExtra("user", new Gson().toJson(user));
                    intent.putExtra("file", filePath);
                    startActivity(intent);

                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });
    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

    }

    @Override
    public void setSpecialities(List<Speciality> specialities) {
        List<Speciality> items = new ArrayList<Speciality>();
        specialities.add(0, new Speciality(0, "Choose Your Speciality"));


        final ArrayAdapter<Speciality> spinnerArrayAdapter = new ArrayAdapter<Speciality>(
                getActivity(), R.layout.spinner_item, specialities) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(getResources().getColor(R.color.signInGray));
                } else {
                    tv.setTextColor(getResources().getColor(R.color.signInGray));
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        speciality.setAdapter(spinnerArrayAdapter);

        speciality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Speciality selectedItemText = (Speciality) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    selectedSpeciality = selectedItemText.getName();
                    user.setSpecialityId(selectedItemText.getId());
                    // Notify the selected item text
//                    Toast.makeText
//                            (getActivity().getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
//                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    if (data != null && data.getData() != null) {
                        Uri selectedImage = data.getData();
                        Log.d("here", "data" + selectedImage);
                        filePath = ImageUploader.getPath(getActivity(), selectedImage);
                    } else if (data != null && data.getExtras() != null && data.getExtras().get("data") != null) {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        Uri selectedImage = ImageUploader.getImageUri(getActivity().getApplicationContext(), photo);
                        Log.d("here", "data" + selectedImage);
                        filePath = ImageUploader.getPath(getActivity(), selectedImage);
                    }
                   // attachmentDisplay.setVisibility(View.VISIBLE);
                    imageText.setText(filePath.substring(filePath.lastIndexOf("/") + 1));
                    image.setImageBitmap(BitmapFactory.decodeFile(filePath));
                }

                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedFile = data.getData();
                    filePath = ImageUploader.getPath(getActivity(), selectedFile);
                    Log.d("here", "file path " + filePath);
                    imageText.setText(filePath.substring(filePath.lastIndexOf("/") + 1));
                    image.setImageBitmap(BitmapFactory.decodeFile(filePath));
                }
                break;
        }
    }

    private void requestGalleryPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
            openFilePicker();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openFilePicker();
        }
    }

    private void openFilePicker() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickPhoto.setType("image/*");
            startActivityForResult(pickPhoto, 1);
        } else {
            Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickPhoto.setType("image/*");
            startActivityForResult(pickPhoto, 1);
        }
    }
}
