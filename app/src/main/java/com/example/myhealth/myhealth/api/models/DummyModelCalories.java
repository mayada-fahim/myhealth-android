package com.example.myhealth.myhealth.api.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hossam Elsawy on 30/05/18.
 */
public class DummyModelCalories implements Parcelable {
    private int image;
    private String Title;

    public static final Creator<DummyModelCalories> CREATOR = new Creator<DummyModelCalories>() {
        @Override
        public DummyModelCalories createFromParcel(Parcel in) {
            return new DummyModelCalories(in);
        }

        @Override
        public DummyModelCalories[] newArray(int size) {
            return new DummyModelCalories[size];
        }
    };

    protected DummyModelCalories(Parcel in) {
        image = in.readInt();
        Title = in.readString();
    }

    public DummyModelCalories() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(image);
        dest.writeString(Title);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
