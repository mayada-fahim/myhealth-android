package com.example.myhealth.myhealth.screen.Clinic;

import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Category;
import com.example.myhealth.myhealth.api.responses.ListCategoriesResponse;
import com.example.myhealth.myhealth.api.services.CategoryService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class ClinicPresenter {

    private ClinicView mView;

    public ClinicPresenter(ClinicView clinicView) {
        mView = clinicView;
    }

    public void getCategories(Integer page, String search) {
        CategoryService service = Network.getInstance().createService(CategoryService.class);

        service.getCategories(page, search).enqueue(new Callback<ListCategoriesResponse>() {
            @Override
            public void onResponse(Call<ListCategoriesResponse> call, Response<ListCategoriesResponse> response) {
                ListCategoriesResponse responseBody = response.body();
                List<Category> categories = new ArrayList<Category>();
                if (responseBody != null) {
                    categories = responseBody.getCategories();
                    Log.e("here", categories.toString());
                }
                mView.setCategories(categories);

            }

            @Override
            public void onFailure(Call<ListCategoriesResponse> call, Throwable t) {

            }
        });
    }
}
