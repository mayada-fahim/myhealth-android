package com.example.myhealth.myhealth.screen.Doctor;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Category;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.CircleTransform;
import com.example.myhealth.myhealth.screen.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */
public class DoctorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<User> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    private Context context;

    public DoctorAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        // this.mData = data;
    }

    public void clearData() {
        mData.clear();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.doctor_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bindViewOnItem(position);
    }

    public void setDoctors(List<User> doctors) {
        if (mData == null) {
            mData = doctors;
        } else {
            // mData.clear();
            mData.addAll(doctors);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public void clear() {
        if (mData == null) return;
        int size = this.mData.size();
        mData.clear();
        // notifyDataSetChanged();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView doctorName;
        private TextView speciality;
        private ImageView doctorImage;
        private ImageView likeRed;


        public ViewHolder(View itemView) {
            super(itemView);
            doctorName = (TextView) itemView.findViewById(R.id.doctorName);
            speciality = (TextView) itemView.findViewById(R.id.speciality);
            doctorImage = (ImageView) itemView.findViewById(R.id.doctorImage);
            itemView.setOnClickListener(this);

        }

        void bindViewOnItem(final int position) {
            User user = mData.get(position);
            doctorName.setText(user.getName());
            if (user.getSpeciality() != null) {
                speciality.setText(user.getSpeciality().getName());
            }

            if (user.getProfileImage() != null) {
                Picasso.with(context).load(Constants.BASE_URL + user.getProfileImage()).transform(new CircleTransform())
                        .into(doctorImage);
            }


        }

        @Override
        public void onClick(View view) {

            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


    public User getItem(int position) {
        return mData.get(position);
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public abstract static class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
        public String TAG = EndlessRecyclerOnScrollListener.class.getSimpleName();

        private int previousTotal = 0; // The total number of items in the dataset after the last load
        private boolean loading = true; // True if we are still waiting for the last set of data to load.
        private int visibleThreshold = 5; // The minimum amount of items to have below your current scroll position before loading more.
        int firstVisibleItem, visibleItemCount, totalItemCount;

        private int current_page = 1;

        private LinearLayoutManager mLinearLayoutManager;

        public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
            this.mLinearLayoutManager = linearLayoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            visibleItemCount = recyclerView.getChildCount();
            totalItemCount = mLinearLayoutManager.getItemCount();
            firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)) {
                // End has been reached

                // Do something
                current_page++;

                onLoadMore(current_page);

                loading = true;
            }
        }

        public abstract void onLoadMore(int current_page);
    }

}