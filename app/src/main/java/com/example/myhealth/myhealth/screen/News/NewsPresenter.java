package com.example.myhealth.myhealth.screen.News;

import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.News;
import com.example.myhealth.myhealth.api.responses.ListArticlesResponse;
import com.example.myhealth.myhealth.api.responses.ListNewsResponse;
import com.example.myhealth.myhealth.api.services.ArticleService;
import com.example.myhealth.myhealth.api.services.NewsService;
import com.example.myhealth.myhealth.screen.Article.ArticleView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 5/6/2018.
 */

public class NewsPresenter {
    private NewsView mView;

    public NewsPresenter(NewsView newsView) {
        mView = newsView;
    }

    public void getNews(Integer page) {
        NewsService service = Network.getInstance().createService(NewsService.class);

        service.getNews(page).enqueue(new Callback<ListNewsResponse>() {
            @Override
            public void onResponse(Call<ListNewsResponse> call, Response<ListNewsResponse> response) {

                ListNewsResponse responseBody = response.body();
                List<News> news = new ArrayList<News>();
                if (responseBody != null) {
                    news = responseBody.getNews();
                    Log.e("here", news.toString());
                }
                mView.setNews(news);

            }

            @Override
            public void onFailure(Call<ListNewsResponse> call, Throwable t) {
                Log.e("here6", t.getMessage());
            }
        });
    }
}
