package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Video;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/1/2018.
 */

public class ListVideosResponse {

    @SerializedName("Results")
    @Expose
    public List<Video> videos;

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }
}
