package com.example.myhealth.myhealth.screen.Doctor;

import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.api.responses.ListArticlesResponse;
import com.example.myhealth.myhealth.api.responses.ListDoctorsResponse;
import com.example.myhealth.myhealth.api.services.ArticleService;
import com.example.myhealth.myhealth.api.services.UserService;
import com.example.myhealth.myhealth.screen.Article.ArticleView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */

public class DoctorPresenter {


    private DoctorView mView;

    public DoctorPresenter(DoctorView doctorView) {
        mView = doctorView;
    }

    public void getDoctors(Integer page, String search) {
        UserService service = Network.getInstance().createService(UserService.class);
        service.getDoctors(page, search).enqueue(new Callback<ListDoctorsResponse>() {
            @Override
            public void onResponse(Call<ListDoctorsResponse> call, Response<ListDoctorsResponse> response) {
                ListDoctorsResponse responseBody = response.body();
                List<User> doctors = new ArrayList<User>();
                if (responseBody != null) {
                    doctors = responseBody.getDoctors();
                    Log.e("here2", "here3");
                    Log.e("here", doctors.toString());
                }
                mView.setDoctors(doctors);

            }

            @Override
            public void onFailure(Call<ListDoctorsResponse> call, Throwable t) {
                Log.e("here6", t.getMessage());
            }
        });
    }

}
