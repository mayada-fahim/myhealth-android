package com.example.myhealth.myhealth.api.services;

import com.example.myhealth.myhealth.api.responses.ListDiseasesResponse;
import com.example.myhealth.myhealth.api.responses.ShowDiseaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Mayada Fahim on 4/21/2018.
 */

public interface DiseaseService {

    @GET("diseases/{page}")
    Call<ListDiseasesResponse> getDiseases(@Path("page") int page, @Query("search") String search);

    @GET("diseases/symptoms/{page}")
    Call<ListDiseasesResponse> getDiseasesBySymptoms(@Path("page") int page, @Query("symptomsIds") String symptomsIds);

    @GET("diseases/details/{id}")
    Call<ShowDiseaseResponse> getDiseaseById(@Path("id") int id);


}
