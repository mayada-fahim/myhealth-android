package com.example.myhealth.myhealth.screen.Video;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Video;


import java.util.List;

/**
 * Created by Mayada Fahim on 5/1/2018.
 */

public class VideoFragment extends Fragment implements VideoAdapter.ItemClickListener, VideoView {

    private RecyclerView recyclerView;
    private VideoAdapter videoAdapter;
    private VideoPresenter videoPresenter;
    private int page = 1;
    private int categoryId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.video_fragment, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.videos);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        Bundle bundle = getArguments();
        categoryId = bundle.getInt("categoryId");

        recyclerView.setOnScrollListener(new VideoAdapter.EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page = current_page;
                getVideos();
            }
        });
        videoAdapter = new VideoAdapter(getActivity());
        videoPresenter = new VideoPresenter(this);
        recyclerView.setAdapter(videoAdapter);
        getVideos();
        return rootView;
    }

    private void getVideos() {
        videoPresenter.getVideos(page, categoryId);
    }

    @Override
    public void setVideos(List<Video> videos) {
        Log.e("here", "here");
        videoAdapter.setVideos(videos);
        videoAdapter.setClickListener(this);
    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

    }

    @Override
    public void onItemClick(View view, int position) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videoAdapter.getItem(position).getUrl())));
        Log.i("Video", "Video Playing....");

    }

    @Override
    public void OnShareClick(View view, int position) {
        Intent share = new Intent(Intent.ACTION_SEND);

        share.putExtra(android.content.Intent.EXTRA_TEXT, videoAdapter.getItem(position).getUrl() + " From MyHealth");
        share.setType("text/plain");

//
        startActivity(Intent.createChooser(share, "MyHealth"));
    }
}
