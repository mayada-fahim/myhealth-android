package com.example.myhealth.myhealth.screen.Video;

import com.example.myhealth.myhealth.api.models.Video;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/1/2018.
 */

public interface VideoView {

    public void setVideos(List<Video> videos);
    public void error(String message);
    public void success();
}
