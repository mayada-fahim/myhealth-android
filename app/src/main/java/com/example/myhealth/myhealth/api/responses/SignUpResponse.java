package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/30/2018.
 */

public class SignUpResponse {

    @SerializedName("user")
    @Expose
    public User user;

    @SerializedName("error")
    @Expose
    public int error;

    @SerializedName("reasons")
    @Expose
    public List<String> reasons;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public List<String> getReasons() {
        return reasons;
    }

    public void setReasons(List<String> reasons) {
        this.reasons = reasons;
    }
}
