package com.example.myhealth.myhealth.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayada Fahim on 12/23/2017.
 */

public class AddTokenResponse {

    @SerializedName("id")
    @Expose
    public Integer id;

    @SerializedName("token")
    @Expose
    public String token;
}
