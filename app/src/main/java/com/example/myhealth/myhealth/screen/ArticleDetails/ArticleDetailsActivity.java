package com.example.myhealth.myhealth.screen.ArticleDetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.screen.AToZ.AToZActivity;
import com.example.myhealth.myhealth.screen.Article.ArticleActivity;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.Disease.DiseaseFragment;
import com.google.gson.Gson;

/**
 * Created by Mayada Fahim on 4/30/2018.
 */

public class ArticleDetailsActivity extends BaseActivity {

    private TextView title;
    private ImageView back;
    private ImageView search;
    private Article article;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_details_activity);
        title = (TextView)findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        search.setVisibility(View.GONE);
        String jsonMyObject;
        Bundle extras = getIntent().getExtras();
        jsonMyObject = extras.getString("article");
        article = new Gson().fromJson(jsonMyObject, Article.class);
        ArticleDetailsFragment articleDetailsFragment = ArticleDetailsFragment.newInstance(new Gson().toJson(article)); ;
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.articleDetailsActivity, articleDetailsFragment, ArticleDetailsFragment.class.getSimpleName())
                .commit();;
        title.setText(article.getTitle());
        setBackClickListener();
    }

    private void  setBackClickListener()
    {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
    }
}
