package com.example.myhealth.myhealth.screen.foodCalories;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.BMIRepresent;


/**
 * Created by Hossam Elsawy on 29/05/18.
 */
public class BMICalculatePresenter {


    public final static double UNDER_WEIGHT = 18.5;
    public final static double NORMAL_WEIGHT_LOWER_LIMIT = 18.5;
    public final static double NORMAL_WEIGHT_UPPER_LIMIT = 24.9;
    public final static double OVER_WEIGHT_LOWER_LIMIT = 25;
    public final static double OVER_WEIGHT_UPPER_LIMIT = 29.9;
    public final static double OBESITY = 30;
    private BMICalculateView mView;
    private Context context;


    public BMICalculatePresenter(BMICalculateView bmiCalculateView, Context context) {
        mView = bmiCalculateView;
        this.context = context;

    }


    public void calculateBmiResult(Double height, Double weight) {

        BMIRepresent bmiRepresent;
        height = height / 100;
        double bmiResult = weight / (height * height);

        if (bmiResult <= UNDER_WEIGHT) {

            bmiRepresent = new BMIRepresent(context.getString(R.string.thin), context.getString(R.string.bmi_over_weight), R.drawable.lose);

        } else if (bmiResult >= NORMAL_WEIGHT_LOWER_LIMIT && bmiResult <= NORMAL_WEIGHT_UPPER_LIMIT) {

            bmiRepresent = new BMIRepresent("You are very in optimum condition ", context.getString(R.string.bmi_over_weight), R.drawable.perfect_weight);
        } else if (bmiResult >= OVER_WEIGHT_LOWER_LIMIT && bmiResult <= OVER_WEIGHT_UPPER_LIMIT) {


            bmiRepresent = new BMIRepresent("you eat To match", context.getString(R.string.bmi_over_weight), R.drawable.over_weight);

        } else {
            bmiRepresent = new BMIRepresent("Your case is over Weight", "Causes of obesity Obesity is generally caused by consuming more calories – particularly those in fatty and sugary foods – than you burn off through physical activity. The excess energy is stored by the body as fat. ", R.drawable.over_weight);
        }


        mView.getBMIResult(bmiRepresent);

    }

    public boolean validateInputParam(EditText weight, EditText height) {
        boolean isSucess = true;

        if (TextUtils.isEmpty(weight.getText())) {
            mView.setErrorWeight();
            isSucess = false;

        }
        if (TextUtils.isEmpty(height.getText())) {

            mView.setErrorHeight();
            isSucess = false;

        }

        return isSucess;

    }

}
