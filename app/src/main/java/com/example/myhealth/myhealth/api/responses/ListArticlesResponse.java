package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class ListArticlesResponse {

    @SerializedName("Results")
    @Expose
    public List<Article> articles;

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
