package com.example.myhealth.myhealth.screen.Symptoms;

import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.models.Symptom;
import com.example.myhealth.myhealth.api.responses.ListDiseasesResponse;
import com.example.myhealth.myhealth.api.responses.ListSymptomsResponse;
import com.example.myhealth.myhealth.api.services.DiseaseService;
import com.example.myhealth.myhealth.api.services.SymptomService;
import com.example.myhealth.myhealth.screen.AToZ.AToZView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */

public class SymptomPresenter {

    private SymptomView mView;

    public SymptomPresenter(SymptomView symptomView) {
        mView = symptomView;
    }

    public void getDiseases(Integer page, String symptomsIds) {
        DiseaseService service = Network.getInstance().createService(DiseaseService.class);

        service.getDiseasesBySymptoms(page, symptomsIds).enqueue(new Callback<ListDiseasesResponse>() {
            @Override
            public void onResponse(Call<ListDiseasesResponse> call, Response<ListDiseasesResponse> response) {
                ListDiseasesResponse responseBody = response.body();
                List<Disease> diseases = new ArrayList<Disease>();
                if (responseBody != null) {
                    diseases = responseBody.getDiseases();
                    Log.e("here", diseases.toString());
                }
                mView.setDiseases(diseases);

            }

            @Override
            public void onFailure(Call<ListDiseasesResponse> call, Throwable t) {

            }
        });
    }

    public void getSymptoms() {
        SymptomService service = Network.getInstance().createService(SymptomService.class);

        service.getSymptoms().enqueue(new Callback<ListSymptomsResponse>() {
            @Override
            public void onResponse(Call<ListSymptomsResponse> call, Response<ListSymptomsResponse> response) {
                ListSymptomsResponse responseBody = response.body();
                List<Symptom> symptoms = new ArrayList<Symptom>();
                if (responseBody != null) {
                    symptoms = responseBody.getSymptoms();
                    Log.e("here", symptoms.toString());
                }
                mView.setSymptoms(symptoms);

            }

            @Override
            public void onFailure(Call<ListSymptomsResponse> call, Throwable t) {

            }
        });
    }
}
