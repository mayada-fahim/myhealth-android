package com.example.myhealth.myhealth.screen.foodCalories;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.home.HomeActivity;

import java.util.ArrayList;
import java.util.List;

public class FoodCaloriesActivity extends BaseActivity {

    private RelativeLayout headerlayout;
    private TabLayout tabs;
    private FrameLayout contentframe;
    private ViewPager viewpager;
    private ImageView back;
    private TextView title;
    private ImageView search;
    private View searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_calories);

        initUi();

    }

    private void initUi() {
        title = findViewById(R.id.headerTtitle);
        back = findViewById(R.id.back);
        search = findViewById(R.id.search);
        searchView = findViewById(R.id.searchView);
        search.setVisibility(View.GONE);
        searchView.setVisibility(View.GONE);
        title.setText(R.string.food_calories_title);
        setBackClickListener();
        viewpager = findViewById(R.id.viewpager);
        contentframe = findViewById(R.id.content_frame);
        tabs = findViewById(R.id.tabs);
        headerlayout = findViewById(R.id.header_layout);

        setupViewPager(viewpager);
        tabs.setupWithViewPager(viewpager);
    }

    private void setBackClickListener() {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, HomeActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

            }
        });


    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BMICalculateFragment(), getString(R.string.bmi_calculate_header));
        adapter.addFragment(new CaloriesListFragment(), getString(R.string.calories_list_header));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
