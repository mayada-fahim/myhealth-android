package com.example.myhealth.myhealth.screen.foodCalories;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.DummyModelDetailsCalories;
import com.example.myhealth.myhealth.screen.News.NewsAdapter;

import java.util.List;

public class CaloriesDetialsFragment extends Fragment implements CaloriesDetailsView {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;
    private RecyclerView caloriesdetailsrv;
    private android.widget.ProgressBar progressBar;
    private CaloriesDetialsAdapter caloriesDetialsAdapter;
    private CaloriesDetialsPresenter caloriesDetialsPresenter;
    private int page = 1;


    public CaloriesDetialsFragment() {
        // Required empty public constructor
    }

    public static CaloriesDetialsFragment newInstance(String param1, String param2) {
        CaloriesDetialsFragment fragment = new CaloriesDetialsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_calories_detials, container, false);

        progressBar = view.findViewById(R.id.progressBar);
        caloriesdetailsrv = view.findViewById(R.id.calories_details_rv);


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        caloriesdetailsrv.setLayoutManager(layoutManager);
        caloriesdetailsrv.addOnScrollListener(new NewsAdapter.EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page = current_page;
                getCaloriesList();
            }
        });


        caloriesDetialsAdapter = new CaloriesDetialsAdapter(getContext());
        caloriesDetialsPresenter = new CaloriesDetialsPresenter(this);
        caloriesdetailsrv.setAdapter(caloriesDetialsAdapter);

        getCaloriesList();

        return view;
    }

    @Override
    public void setCaloriesDetials(List<DummyModelDetailsCalories> dummyModelCalories) {

        progressBar.setVisibility(View.GONE);

        caloriesDetialsAdapter.setModelCalories(dummyModelCalories);
    }


    private void getCaloriesList() {

        progressBar.setVisibility(View.VISIBLE);

        caloriesDetialsPresenter.getCaloriesDeatilsModel();


    }


}
