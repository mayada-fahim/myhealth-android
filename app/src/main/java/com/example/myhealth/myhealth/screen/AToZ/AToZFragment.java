package com.example.myhealth.myhealth.screen.AToZ;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.screen.Article.ArticleActivity;
import com.example.myhealth.myhealth.screen.Disease.DiseaseActivity;
import com.google.gson.Gson;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.List;

import pl.droidsonroids.gif.GifTextView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mayada Fahim on 4/21/2018.
 */

public class AToZFragment extends Fragment implements DiseaseAdapter.ItemClickListener, AToZView {

    private RecyclerView recyclerView;
    private DiseaseAdapter diseaseAdapter;
    private AToZPresenter aToZPresenter;
    private int page = 1;
    private String search;
    private ProgressBar progressBar;
    private ImageView noResults;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static AToZFragment newInstance() {
        return new AToZFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.a_to_z_fragment, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.diseases);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        noResults = (ImageView) rootView.findViewById(R.id.noResult);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setOnScrollListener(new DiseaseAdapter.EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page  = current_page;
                getDiseases(search, page);
            }
        });
        diseaseAdapter = new DiseaseAdapter(getActivity());
        aToZPresenter = new AToZPresenter(this);
        recyclerView.setAdapter(diseaseAdapter);
        recyclerView.addItemDecoration(new StickyRecyclerHeadersDecoration(diseaseAdapter));
        search = "";
        getDiseases(search, page);
        return rootView;
    }

    public void getDiseases(String text, int pageNum)
    {
        noResults.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        search = text;
        page = pageNum;
        aToZPresenter.getDiseases(pageNum, text);
    }

    @Override
    public void setDiseases(List<Disease> diseases) {
        if(diseases.size() == 0)
        {
            noResults.setVisibility(View.VISIBLE);
        }else {
            noResults.setVisibility(View.GONE);
        }
        diseaseAdapter.setDiseases(diseases);
        diseaseAdapter.setClickListener(this);
        progressBar.setVisibility(View.GONE);
    }

    public void clearData() {
        diseaseAdapter.clearData();
    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

    }

    @Override
    public void onItemClick(View view, int position) {
        Disease disease = diseaseAdapter.getItem(position);
        Log.e("here", disease.getChilds().size() +"");
        if(disease.getChilds().size() != 0)
        {
            Intent intent = new Intent(getActivity(), DiseaseActivity.class);
            intent.putExtra("disease", new Gson().toJson(disease));
            startActivity(intent);

            getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
        }else {
            Intent intent = new Intent(getActivity(), ArticleActivity.class);
            intent.putExtra("disease", new Gson().toJson(disease));
            startActivity(intent);

            getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }
}
