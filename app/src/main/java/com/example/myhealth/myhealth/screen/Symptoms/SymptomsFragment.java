package com.example.myhealth.myhealth.screen.Symptoms;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.models.Symptom;
import com.example.myhealth.myhealth.screen.AToZ.AToZFragment;
import com.example.myhealth.myhealth.screen.AToZ.AToZPresenter;
import com.example.myhealth.myhealth.screen.AToZ.AToZView;
import com.example.myhealth.myhealth.screen.AToZ.DiseaseAdapter;
import com.example.myhealth.myhealth.screen.Disease.DiseaseActivity;
import com.example.myhealth.myhealth.screen.DiseaseDetails.DiseaseDetailsActivity;
import com.google.gson.Gson;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */

public class SymptomsFragment extends Fragment implements DiseaseAdapter.ItemClickListener, SymptomView {

    private RecyclerView recyclerView;
    private DiseaseAdapter diseaseAdapter;
    private SymptomPresenter symptomPresenter;
    private int page = 1;
    private String symptomIds = "";
    private MultiAutoCompleteTextView simpleMultiAutoCompleteTextView;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SymptomsFragment newInstance() {
        return new SymptomsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.symptoms_fragment, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.diseases);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setOnScrollListener(new DiseaseAdapter.EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page = current_page;
                getDiseases(symptomIds, page);
            }
        });
        diseaseAdapter = new DiseaseAdapter(getActivity());
        symptomPresenter = new SymptomPresenter(this);
        recyclerView.setAdapter(diseaseAdapter);
        recyclerView.addItemDecoration(new StickyRecyclerHeadersDecoration(diseaseAdapter));
        getDiseases(symptomIds, page);
        getSymptoms();
        simpleMultiAutoCompleteTextView = (MultiAutoCompleteTextView) rootView.findViewById(R.id.simpleMultiAutoCompleteTextView);
        return rootView;
    }


    public void getDiseases(String text, int pageNum) {
        progressBar.setVisibility(View.VISIBLE);
        symptomIds = text;
        page = pageNum;
        symptomPresenter.getDiseases(pageNum, text);
    }

    public void getSymptoms() {
        symptomPresenter.getSymptoms();
    }

    @Override
    public void setDiseases(List<Disease> diseases) {
        progressBar.setVisibility(View.GONE);
        diseaseAdapter.setDiseases(diseases);
        diseaseAdapter.setClickListener(this);
    }

    public void clearData() {
        diseaseAdapter.clearData();
    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), DiseaseDetailsActivity.class);
        intent.putExtra("disease", new Gson().toJson(diseaseAdapter.getItem(position)));
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    @Override
    public void setSymptoms(List<Symptom> symptoms) {
        // set adapter to fill the data in suggestion list
        final ArrayAdapter<Symptom> symptomsList = new ArrayAdapter<Symptom>(getActivity(), android.R.layout.simple_list_item_1, symptoms);
        simpleMultiAutoCompleteTextView.setAdapter(symptomsList);

        // set threshold value 1 that help us to start the searching from first character
        simpleMultiAutoCompleteTextView.setThreshold(1);
        // set tokenizer that distinguish the various substrings by comma
        simpleMultiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        simpleMultiAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                symptomIds += symptomsList.getItem(position).getId() + ",";
                clearData();
                getDiseases(symptomIds, 1);
            }
        });

        simpleMultiAutoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener(){

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.e("here", v.getText().toString());
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.e("here", v.getText().toString());
                    return true;
                }
                return false;
            }

        });
    }
}
