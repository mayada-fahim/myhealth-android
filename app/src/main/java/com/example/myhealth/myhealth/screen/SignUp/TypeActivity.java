package com.example.myhealth.myhealth.screen.SignUp;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.SignIn.SignInFragment;

/**
 * Created by Mayada Fahim on 4/29/2018.
 */

public class TypeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.type_activity);
        TypeFragment typeFragment = new TypeFragment();
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.typeActivity, typeFragment, TypeFragment.class.getSimpleName())
                .commit();
    }
}
