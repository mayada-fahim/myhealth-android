package com.example.myhealth.myhealth.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import static android.support.v4.util.Preconditions.checkNotNull;

/**
 * Created by Mayada Fahim on 12/16/2017.
 */

public class PreferencesUtils {
    @SuppressWarnings("CanBeFinal")
    private SharedPreferences preferences ;
    private static PreferencesUtils sInstance;

    private PreferencesUtils(@NonNull Context context, @NonNull String sharedPrefName) {
        checkNotNull(context);
        checkNotNull(sharedPrefName);
        this.preferences= context.getSharedPreferences(sharedPrefName, 0);
    }

    public static synchronized PreferencesUtils getInstance(@NonNull Context context,@NonNull String sharedPrefName) {
        checkNotNull(context);
        checkNotNull(sharedPrefName);
        if (sInstance == null) {
            sInstance = new PreferencesUtils(context,sharedPrefName);
        }
        return sInstance;
    }

    public SharedPreferences getPreferences() {
        return preferences;
    }
}
