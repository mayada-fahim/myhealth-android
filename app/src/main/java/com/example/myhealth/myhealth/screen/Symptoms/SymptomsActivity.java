package com.example.myhealth.myhealth.screen.Symptoms;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.screen.AToZ.AToZFragment;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.home.HomeActivity;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */

public class SymptomsActivity extends BaseActivity {

    private TextView title;
    private ImageView back;
    private ImageView search;
    private View searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.symptoms_activtiy);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        title = (TextView) findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        searchView  =(View) findViewById(R.id.searchView);
        SymptomsFragment symptomsFragment = new SymptomsFragment();
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.symptomsActivity, symptomsFragment, SymptomsFragment.class.getSimpleName())
                .commit();

        title.setText("Symptoms");
        search.setVisibility(View.GONE);
        searchView.setVisibility(View.GONE);
        setBackClickListener();
    }

    private void setBackClickListener() {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, HomeActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
    }
}
