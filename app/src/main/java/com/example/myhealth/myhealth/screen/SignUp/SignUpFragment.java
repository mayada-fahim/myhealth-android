package com.example.myhealth.myhealth.screen.SignUp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.models.Speciality;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.SignIn.SignInActivity;
import com.example.myhealth.myhealth.screen.SignIn.SignInFragment;
import com.example.myhealth.myhealth.screen.home.HomeActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayada Fahim on 4/29/2018.
 */

public class SignUpFragment extends Fragment implements SignUpView {

    private EditText name;
    private TextView nameError;
    private EditText email;
    private TextView emailError;
    private EditText password;
    private TextView passwordError;
    private EditText confirmPassword;
    private TextView confirmPasswordError;
    private TextView male;
    private TextView female;
    private EditText mobileNumber;
    private Button signUp;
    private boolean error = false;
    private User user;
    private SignUpPresenter signUpPresenter;
    private ProgressDialog dialog;
    //private ProgressBar progressBar;
    private String file;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SignUpFragment newInstance(int type, String user, String file) {
        SignUpFragment signUpFragment = new SignUpFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putString("user", user);
        bundle.putString("file", file);
        signUpFragment.setArguments(bundle);

        return signUpFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sign_up_fragment, container, false);
        name = (EditText) rootView.findViewById(R.id.name);
        nameError = (TextView) rootView.findViewById(R.id.name_error);
        email = (EditText) rootView.findViewById(R.id.email);
        emailError = (TextView) rootView.findViewById(R.id.email_error);
        password = (EditText) rootView.findViewById(R.id.password);
        passwordError = (TextView) rootView.findViewById(R.id.password_error);
        confirmPassword = (EditText) rootView.findViewById(R.id.confirm_password);
        confirmPasswordError = (TextView) rootView.findViewById(R.id.password_confirm_error);
        password.setTypeface(Typeface.DEFAULT);
        password.setTransformationMethod(new PasswordTransformationMethod());
        password.setTypeface(Typeface.DEFAULT);
        confirmPassword.setTransformationMethod(new PasswordTransformationMethod());
        male = (TextView) rootView.findViewById(R.id.male);
        female = (TextView) rootView.findViewById(R.id.female);
        mobileNumber = (EditText) rootView.findViewById(R.id.mobile);
        signUp = (Button) rootView.findViewById(R.id.signup_button);
        //progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        dialog = new ProgressDialog(getActivity());
        String userStr = getArguments().getString("user");
        user = new Gson().fromJson(userStr, User.class);
        Log.e("here", getArguments().getInt("type") + "");
        user.setTypeId(getArguments().getInt("type"));
        file = getArguments().getString("file");
        signUpPresenter = new SignUpPresenter(this);
        setGenderClickListener();
        setSignUpClickListener();
        return rootView;
    }

    private void setGenderClickListener() {
        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male.setTextColor(getResources().getColor(R.color.colorPrimary));
                female.setTextColor(getResources().getColor(R.color.signInGray));
                user.setGender(1);
            }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                female.setTextColor(getResources().getColor(R.color.colorPrimary));
                male.setTextColor(getResources().getColor(R.color.signInGray));
                user.setGender(0);
            }
        });
    }

    private void setSignUpClickListener() {
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nameError.setVisibility(View.GONE);
                emailError.setVisibility(View.GONE);
                passwordError.setVisibility(View.GONE);
                confirmPasswordError.setVisibility(View.GONE);
                error = false;
                if (name.getText().toString().matches("")) {
                    error = true;
                    nameError.setVisibility(View.VISIBLE);
                }

                if (email.getText().toString().replaceAll("\\s+", "").matches("")) {
                    error = true;
                    emailError.setText("Please enter your email");
                    emailError.setVisibility(View.VISIBLE);
                }

                if (!email.getText().toString().replaceAll("\\s+", "").matches(emailPattern)) {
                    error = true;
                    emailError.setText("Please enter correct email format");
                    emailError.setVisibility(View.VISIBLE);
                }

                if (password.getText().toString().matches("")) {
                    error = true;
                    passwordError.setText("Please enter your password");
                    passwordError.setVisibility(View.VISIBLE);
                }

                if (!password.getText().toString().equals(confirmPassword.getText().toString())) {
                    error = true;
                    confirmPasswordError.setText("Password doesnot match");
                    confirmPasswordError.setVisibility(View.VISIBLE);
                }

                if (!error) {
                    user.setName(name.getText().toString());
                    user.setEmail(email.getText().toString());
                    user.setPassword(password.getText().toString());
                    user.setMobileNumber(mobileNumber.getText().toString());
                   // progressBar.setVisibility(View.VISIBLE);
                    dialog.setMessage("Loading");
                    dialog.show();
                   // signUp.setVisibility(View.GONE);
                    signUpPresenter.signUp(user, file, getContext());
                }
            }
        });
    }

    @Override
    public void error(String message) {
        emailError.setVisibility(View.VISIBLE);
        emailError.setText(message);
       // progressBar.setVisibility(View.GONE);
        dialog.dismiss();
        signUp.setVisibility(View.VISIBLE);
    }

    @Override
    public void success() {
       // progressBar.setVisibility(View.GONE);
        dialog.dismiss();
        signUp.setVisibility(View.VISIBLE);
        Intent intent = new Intent(getActivity(), SignInActivity.class);
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void setSpecialities(List<Speciality> specialities) {

    }
}
