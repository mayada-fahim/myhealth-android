package com.example.myhealth.myhealth.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/21/2018.
 */

public class Disease {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("name_ar")
    public String nameArabic;


    @SerializedName("medical_name")
    public String medicalName;

    @SerializedName("medical_name_ar")
    public String medicalNameArabic;

    @SerializedName("childs")
    public List<Disease> childs;

    @SerializedName("symptoms")
    public List<Symptom> symptoms;

    @SerializedName("description")
    public String description;

    @SerializedName("description_ar")
    public String descriptionArabic;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionArabic() {
        return descriptionArabic;
    }

    public void setDescriptionArabic(String descriptionArabic) {
        this.descriptionArabic = descriptionArabic;
    }

    public List<Symptom> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(List<Symptom> symptoms) {
        this.symptoms = symptoms;
    }

    public Disease(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameArabic() {
        return nameArabic;
    }

    public void setNameArabic(String nameArabic) {
        this.nameArabic = nameArabic;
    }

    public String getMedicalName() {
        return medicalName;
    }

    public void setMedicalName(String medicalName) {
        this.medicalName = medicalName;
    }

    public String getMedicalNameArabic() {
        return medicalNameArabic;
    }

    public void setMedicalNameArabic(String medicalNameArabic) {
        this.medicalNameArabic = medicalNameArabic;
    }

    public List<Disease> getChilds() {
        return childs;
    }

    public void setChilds(List<Disease> childs) {
        this.childs = childs;
    }
}
