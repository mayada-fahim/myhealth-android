package com.example.myhealth.myhealth.api.services;

import com.example.myhealth.myhealth.api.responses.ListArticlesResponse;
import com.example.myhealth.myhealth.api.responses.ListNewsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Mayada Fahim on 5/6/2018.
 */

public interface NewsService {

    @GET("news/{page}")
    Call<ListNewsResponse> getNews(@Path("page") int page);
}
