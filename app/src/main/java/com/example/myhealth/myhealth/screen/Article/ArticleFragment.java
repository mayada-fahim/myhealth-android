package com.example.myhealth.myhealth.screen.Article;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.screen.ArticleDetails.ArticleDetailsActivity;
import com.example.myhealth.myhealth.screen.Disease.DiseaseActivity;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class ArticleFragment extends Fragment implements ArticleAdapter.ItemClickListener, ArticleView {

    private RecyclerView recyclerView;
    private ArticleAdapter articleAdapter;
    private ArticlePresenter articlePresenter;
    private int page = 1;
    private int diseaseId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ArticleFragment newInstance() {
        return new ArticleFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.article_fragment, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.articles);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        Bundle bundle = getArguments();
        diseaseId = bundle.getInt("diseaseId");

        recyclerView.setOnScrollListener(new ArticleAdapter.EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page  = current_page;
                getArticles();
            }
        });
        articleAdapter = new ArticleAdapter(getActivity());
        articlePresenter = new ArticlePresenter(this);
        recyclerView.setAdapter(articleAdapter);
        getArticles();
        return rootView;
    }

    private void getArticles()
    {
        articlePresenter.getArticles(page, diseaseId);
    }

    @Override
    public void setArticles(List<Article> articles) {
        Log.e("here", "here");
        articleAdapter.setArticles(articles);
        articleAdapter.setClickListener(this);
    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), ArticleDetailsActivity.class);
        intent.putExtra("article", new Gson().toJson(articleAdapter.getItem(position)));
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void OnShareClick(View view, int position) {
        Intent share = new Intent(Intent.ACTION_SEND);

        share.putExtra(android.content.Intent.EXTRA_TEXT, articleAdapter.getItem(position).getContent() + " From MyHealth");
        share.setType("text/plain");

//
        startActivity(Intent.createChooser(share, "MyHealth"));
    }
}
