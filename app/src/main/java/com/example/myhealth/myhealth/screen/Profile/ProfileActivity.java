package com.example.myhealth.myhealth.screen.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.screen.AToZ.AToZActivity;
import com.example.myhealth.myhealth.screen.Article.ArticleFragment;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.home.HomeActivity;
import com.google.gson.Gson;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */

public class ProfileActivity extends BaseActivity {

    private ImageView back;
    private TextView title;
    private ImageView search;
    private View searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        title = (TextView)findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        searchView = (View) findViewById(R.id.searchView);
        search.setVisibility(View.GONE);
        searchView.setVisibility(View.GONE);
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.profileActivity, profileFragment, ProfileFragment.class.getSimpleName())
                .commit();;
        title.setText("Profile");
        setBackClickListener();
    }

    private void  setBackClickListener()
    {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, HomeActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
    }
}
