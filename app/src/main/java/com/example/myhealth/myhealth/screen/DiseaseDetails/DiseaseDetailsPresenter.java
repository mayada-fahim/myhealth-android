package com.example.myhealth.myhealth.screen.DiseaseDetails;

import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.responses.ListDiseasesResponse;
import com.example.myhealth.myhealth.api.responses.ShowDiseaseResponse;
import com.example.myhealth.myhealth.api.services.DiseaseService;
import com.example.myhealth.myhealth.screen.AToZ.AToZView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */

public class DiseaseDetailsPresenter {

    private DiseaseDetailsView mView;

    public DiseaseDetailsPresenter(DiseaseDetailsView diseaseDetailsView) {
        mView = diseaseDetailsView;
    }

    public void getDisease(Integer id) {
        DiseaseService service = Network.getInstance().createService(DiseaseService.class);

        service.getDiseaseById(id).enqueue(new Callback<ShowDiseaseResponse>() {
            @Override
            public void onResponse(Call<ShowDiseaseResponse> call, Response<ShowDiseaseResponse> response) {
                ShowDiseaseResponse responseBody = response.body();
                Log.e("here", responseBody.toString());
                if (responseBody != null) {

                    mView.setDisease(responseBody.getDisease());
                    mView.setArticles(responseBody.getArticles());
                }


            }

            @Override
            public void onFailure(Call<ShowDiseaseResponse> call, Throwable t) {

            }
        });
    }
}
