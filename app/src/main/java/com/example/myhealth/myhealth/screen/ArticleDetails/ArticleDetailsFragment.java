package com.example.myhealth.myhealth.screen.ArticleDetails;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.AToZ.DiseaseAdapter;
import com.example.myhealth.myhealth.screen.Clinic.CategoryAdapter;
import com.example.myhealth.myhealth.screen.Clinic.ClinicPresenter;
import com.example.myhealth.myhealth.screen.Constants;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

/**
 * Created by Mayada Fahim on 4/30/2018.
 */

public class ArticleDetailsFragment extends Fragment {

    private Article article;
    private CarouselView carouselView;
    private TextView title;
    private TextView body;
    private ImageView like;
    private ImageView share;
    private TextView author;
    private TextView date;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ArticleDetailsFragment newInstance(String article) {
        ArticleDetailsFragment articleDetailsFragment = new ArticleDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("article", article);
        articleDetailsFragment.setArguments(bundle);

        return articleDetailsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.article_details_fragment, container, false);
        String userStr = getArguments().getString("article");
        article = new Gson().fromJson(userStr, Article.class);
        carouselView = (CarouselView) rootView.findViewById(R.id.carouselView);
        title = rootView.findViewById(R.id.articleTitle);
        body = rootView.findViewById(R.id.article_body);
        author = rootView.findViewById(R.id.authorName);
        date = (TextView) rootView.findViewById(R.id.date);
        like = rootView.findViewById(R.id.like);
        share = rootView.findViewById(R.id.share);
        title.setText(article.getTitle());
        body.setText(article.getContent());
        date.setText(article.getDate().split(" ")[0]);
        if (article.getUser() != null) {
            author.setText(article.getUser().getName());
        } else {
            author.setText("Unknown");
        }
        carouselView.setPageCount(article.getImages().size());

        carouselView.setImageListener(imageListener);
        setShareClickListener();
        return rootView;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Picasso.with(getContext()).load(Constants.BASE_URL + article.getImages().get(position).getPath())
                    .into(imageView);
            //imageView.setImageResource(Constants.BASE_URL + article.getImages().get(position));
        }
    };

    public void setShareClickListener() {
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(Intent.ACTION_SEND);

                share.putExtra(android.content.Intent.EXTRA_TEXT, article.getContent() + " From MyHealth");
                share.setType("text/plain");

//
                startActivity(Intent.createChooser(share, "MyHealth"));
            }
        });
    }
}
