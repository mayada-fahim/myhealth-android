package com.example.myhealth.myhealth.screen.Disease;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.screen.AToZ.AToZActivity;
import com.example.myhealth.myhealth.screen.AToZ.AToZFragment;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.Constants;
import com.example.myhealth.myhealth.screen.UserHelper;
import com.example.myhealth.myhealth.screen.home.HomeActivity;
import com.google.gson.Gson;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class DiseaseActivity extends BaseActivity {


    private TextView title;
    private ImageView back;
    private Disease disease;
    private View searchView;
    private ImageView search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disease_activity);
        title = (TextView) findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        searchView = (View) findViewById(R.id.searchView);
        search.setVisibility(View.GONE);
        searchView.setVisibility(View.GONE);
        String jsonMyObject;
        Bundle extras = getIntent().getExtras();
        jsonMyObject = extras.getString("disease");
        disease = new Gson().fromJson(jsonMyObject, Disease.class);
        DiseaseFragment diseaseFragment = DiseaseFragment.newInstance(new Gson().toJson(disease.getChilds()));
        Bundle bundle = new Bundle();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.diseaseActivity, diseaseFragment, DiseaseFragment.class.getSimpleName())
                .commit();
        UserHelper userHelper = new UserHelper(this);
        if(userHelper.isUserLoggedIn() && userHelper.getUserType() == Constants.DOCTOR)
        {
            title.setText(disease.getMedicalName());
        }else {
            title.setText(disease.getName());
        }
        setBackClickListener();
    }

    private void setBackClickListener() {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, AToZActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
    }
}
