package com.example.myhealth.myhealth.screen.DiseaseDetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.Disease.DiseaseFragment;
import com.example.myhealth.myhealth.screen.Symptoms.SymptomsActivity;
import com.example.myhealth.myhealth.screen.home.HomeActivity;
import com.google.gson.Gson;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */

public class DiseaseDetailsActivity extends BaseActivity {
    private TextView title;
    private ImageView back;
    private ImageView search;
    private DiseaseDetailsFragment diseaseDetailsFragment;
    private View searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disease_details_activity);
        title = (TextView) findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        searchView = (View) findViewById(R.id.searchView);
        Bundle extras = getIntent().getExtras();
        String jsonMyObject;
        jsonMyObject = extras.getString("disease");
        Disease disease = new Gson().fromJson(jsonMyObject, Disease.class);
        diseaseDetailsFragment = new DiseaseDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("id", disease.getId());
        diseaseDetailsFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.diseaseDetailsActivity, diseaseDetailsFragment, DiseaseDetailsFragment.class.getSimpleName())
                .commit();

        title.setText(disease.getName());
        search.setVisibility(View.GONE);
        searchView.setVisibility(View.GONE);
        setBackClickListener();
    }

    private void setBackClickListener() {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, SymptomsActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
    }
}
