package com.example.myhealth.myhealth.api.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */


public class Category {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("name_ar")
    public String nameArabic;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameArabic() {
        return nameArabic;
    }

    public void setNameArabic(String nameArabic) {
        this.nameArabic = nameArabic;
    }
}
