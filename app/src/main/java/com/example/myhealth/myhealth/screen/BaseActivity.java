package com.example.myhealth.myhealth.screen;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.myhealth.myhealth.R;


/**
 * Created by Mayada Fahim on 12/13/2017.
 */

public class BaseActivity extends AppCompatActivity {

    protected final int ACTION_WIFI_SETTINGS_FLAG = 0;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyboardIfShown();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

    protected boolean isNetworkAvailable() {
//        if (this != null) {
//            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
//            return networkInfo != null && networkInfo.isConnected();
//        }
//        return false;
        return true;
    }

    protected void showNotConnectedAlert() {
        if (this != null) {
//            new CustomAlertDialogUtils.Builder(this)
//                    .setTitle("Check Connection")
//                    .setMessage("Please check your internet connection")
//                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), ACTION_WIFI_SETTINGS_FLAG);
//                        }
//                    })
//                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            if (this != null) {
//                                finish();
//                            }
//                        }
//                    })
//                    .setCancelable(false)
//                    .setIcon(R.drawable.no_wifi)
//                    .show();
        }
    }


    @SuppressWarnings("WeakerAccess")
    protected void hideKeyboardIfShown() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        // Hide The keyboard if showing
        View view = getCurrentFocus();
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
