package com.example.myhealth.myhealth.screen.Video;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Category;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.screen.AToZ.AToZActivity;
import com.example.myhealth.myhealth.screen.Article.ArticleFragment;
import com.example.myhealth.myhealth.screen.BaseActivity;
import com.example.myhealth.myhealth.screen.Clinic.ClinicActivity;
import com.google.gson.Gson;

/**
 * Created by Mayada Fahim on 5/1/2018.
 */

public class VideoActivity extends BaseActivity {

    private ImageView back;
    private TextView title;
    private ImageView search;
    private Category category;
    private View searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_activity);
        title = (TextView)findViewById(R.id.headerTtitle);
        back = (ImageView) findViewById(R.id.back);
        search = (ImageView) findViewById(R.id.search);
        search.setVisibility(View.GONE);
        searchView = (View) findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
        String jsonMyObject;
        Bundle extras = getIntent().getExtras();
        jsonMyObject = extras.getString("category");
        category = new Gson().fromJson(jsonMyObject, Category.class);
        VideoFragment videoFragment = new VideoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("categoryId", category.getId());
        videoFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.videoActivity, videoFragment, VideoFragment.class.getSimpleName())
                .commit();;
        title.setText(category.getName());
        setBackClickListener();
    }

    private void  setBackClickListener()
    {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ClinicActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
    }
}
