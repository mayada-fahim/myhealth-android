package com.example.myhealth.myhealth.screen;

/**
 * Created by ${Mayada} on 4/12/17.
 */

public class Constants {
    public static final String BASE_URL = "https://myhealth.itjanus.com/healthdigest/BackEnd/public/";
    //public static final String API = "https://myhealth.itjanus.com/healthdigest/BackEnd/public/api/v1/mobile/";
    public static final String UPLOAD_URL = "https://myhealth.itjanus.com/healthdigest/BackEnd/public/api/v1/mobile/upload";
    public static final int USER = 4;
    public static final int DOCTOR = 3;

}
