package com.example.myhealth.myhealth.api.services;

import com.example.myhealth.myhealth.api.responses.ListCategoriesResponse;
import com.example.myhealth.myhealth.api.responses.ListSymptomsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */

public interface SymptomService {

    @GET("symptoms")
    Call<ListSymptomsResponse> getSymptoms();
}
