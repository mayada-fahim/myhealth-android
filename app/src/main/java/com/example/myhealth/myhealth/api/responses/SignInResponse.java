package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/30/2018.
 */

public class SignInResponse {

    @SerializedName("user")
    @Expose
    public User user;

    @SerializedName("error")
    @Expose
    public String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
