package com.example.myhealth.myhealth.screen.Doctor;

import com.example.myhealth.myhealth.api.models.Article;
import com.example.myhealth.myhealth.api.models.User;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */

public interface DoctorView {
    public void setDoctors(List<User> doctors);
    public void error(String message);
    public void success();
}
