package com.example.myhealth.myhealth.screen.foodCalories;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.BMIRepresent;
import com.example.myhealth.myhealth.screen.BaseActivity;

public class BMIResultActivity extends BaseActivity {

    public final static String BMI_REPRESENT_MODEL = "bmi_represent_model";
    private RelativeLayout headerlayout;
    private ImageView imagerepresent;
    private TextView title;

    private TextView bmibody;
    private ImageView back;
    private TextView titleheader;
    private ImageView search;
    private View searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi_result);
        bmibody = findViewById(R.id.bmi_body);
        title = findViewById(R.id.title);
        imagerepresent = findViewById(R.id.image_represent);
        headerlayout = findViewById(R.id.header_layout);

        titleheader = findViewById(R.id.headerTtitle);
        back = findViewById(R.id.back);
        search = findViewById(R.id.search);
        searchView = findViewById(R.id.searchView);
        search.setVisibility(View.GONE);
        searchView.setVisibility(View.GONE);
        titleheader.setText(R.string.bmi_result);
        setBackClickListener();


        Bundle extras = getIntent().getExtras();
        if (extras != null) {


            BMIRepresent bmiRepresent = extras.getParcelable(BMI_REPRESENT_MODEL);

            if (bmiRepresent != null) {

                title.setText(bmiRepresent.getTitle());
                bmibody.setText(bmiRepresent.getBody());
                imagerepresent.setImageResource(bmiRepresent.getImageRepresent());

            }


        }


    }

    private void setBackClickListener() {
        final AppCompatActivity activity = this;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, FoodCaloriesActivity.class);
                startActivity(intent);

                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

            }
        });


    }
}
