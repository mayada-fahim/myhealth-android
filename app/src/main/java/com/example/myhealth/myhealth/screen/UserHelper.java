package com.example.myhealth.myhealth.screen;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */

public class UserHelper {

    private Context context;
    private SharedPreferences prefs;

    public UserHelper(Context context) {
        this.context = context;
        prefs = this.context.getSharedPreferences("myHealth", MODE_PRIVATE);
    }

    public boolean isUserLoggedIn() {
        prefs = this.context.getSharedPreferences("myHealth", MODE_PRIVATE);
        return prefs.getString("name", null) != null;
    }

    public String getToken() {
        return prefs.getString("token", null);
    }
    public String getDeviceToken() {
        prefs = this.context.getSharedPreferences("myHealth", MODE_PRIVATE);
        return prefs.getString("deviceToken", null);
    }
    public int getUserType()
    {
        return prefs.getInt("type", 0);
    }
    public int getUserId()
    {
        return prefs.getInt("id", 0);
    }
}
