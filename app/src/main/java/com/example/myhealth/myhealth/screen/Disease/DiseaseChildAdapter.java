package com.example.myhealth.myhealth.screen.Disease;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.screen.AToZ.DiseaseAdapter;
import com.example.myhealth.myhealth.screen.Constants;
import com.example.myhealth.myhealth.screen.UserHelper;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class DiseaseChildAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Disease> mData;
    private LayoutInflater mInflater;
    private DiseaseChildAdapter.ItemClickListener mClickListener;

    private Context context;

    public DiseaseChildAdapter(Context context, List<Disease> data) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.disease_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bindViewOnItem(position);
    }

    public void setDiseases(List<Disease> diseases) {
        if (mData == null) {
            mData = diseases;
        } else {
            mData.clear();
            mData.addAll(diseases);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView diseaseName;


        public ViewHolder(View itemView) {
            super(itemView);
            diseaseName = (TextView) itemView.findViewById(R.id.diseaseName);
            itemView.setOnClickListener(this);

        }

        void bindViewOnItem(final int position) {
            Disease disease = mData.get(position);
            UserHelper userHelper = new UserHelper(context);
            if(userHelper.isUserLoggedIn() && userHelper.getUserType() == Constants.DOCTOR)
            {
                diseaseName.setText(disease.getMedicalName());
            }else {
                diseaseName.setText(disease.getName());
            }
        }

        @Override
        public void onClick(View view) {

            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }
    }

    // allows clicks events to be caught
    public void setClickListener(DiseaseChildAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


    public Disease getItem(int position) {
        return mData.get(position);
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
