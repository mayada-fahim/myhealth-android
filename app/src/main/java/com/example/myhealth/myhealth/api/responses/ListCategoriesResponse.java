package com.example.myhealth.myhealth.api.responses;

import com.example.myhealth.myhealth.api.models.Category;
import com.example.myhealth.myhealth.api.models.Disease;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public class ListCategoriesResponse {


    @SerializedName("Results")
    @Expose
    public List<Category> categories = null;


    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
