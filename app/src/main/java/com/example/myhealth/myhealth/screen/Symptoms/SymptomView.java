package com.example.myhealth.myhealth.screen.Symptoms;

import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.models.Symptom;

import java.util.List;

/**
 * Created by Mayada Fahim on 5/5/2018.
 */

public interface SymptomView {

    public void setDiseases(List<Disease> diseases);

    public void setSymptoms(List<Symptom> symptoms);

    public void error(String message);

    public void success();

}
