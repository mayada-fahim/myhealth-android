package com.example.myhealth.myhealth.screen.DiseaseDetails;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.Disease;
import com.example.myhealth.myhealth.api.models.Symptom;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.List;

/**
 * Created by Mayada Fahim on 12/16/2017.
 */

public class SymptomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private List<Symptom> mData;
    private LayoutInflater mInflater;


    private Context context;

    public SymptomAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        // this.mData = data;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.sypmtom_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bindViewOnItem(position);
    }

    public void setSymptoms(List<Symptom> symptoms) {
        if (mData == null) {
            mData = symptoms;
        } else {
            // mData.clear();
            mData.addAll(symptoms);
        }
        notifyDataSetChanged();
    }
    public void clearData()
    {
        mData.clear();
    }
    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public void clear() {
        if (mData == null) return;
        int size = this.mData.size();
        mData.clear();
        // notifyDataSetChanged();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        private TextView name;


        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.sypmtom_name);

        }

        void bindViewOnItem(final int position) {
            Symptom symptom = mData.get(position);
            name.setText("- " + symptom.getName());
        }

    }



    public Symptom getItem(int position) {
        return mData.get(position);
    }



}


