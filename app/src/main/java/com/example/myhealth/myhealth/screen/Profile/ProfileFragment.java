package com.example.myhealth.myhealth.screen.Profile;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.myhealth.myhealth.R;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.screen.Article.ArticleAdapter;
import com.example.myhealth.myhealth.screen.Article.ArticleFragment;
import com.example.myhealth.myhealth.screen.Article.ArticlePresenter;
import com.example.myhealth.myhealth.screen.CircleTransform;
import com.example.myhealth.myhealth.screen.Constants;
import com.example.myhealth.myhealth.screen.SignIn.SignInActivity;
import com.example.myhealth.myhealth.screen.home.HomeActivity;
import com.example.myhealth.myhealth.screen.home.HomeFragment;
import com.example.myhealth.myhealth.utils.ImageUploader;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mayada Fahim on 5/26/2018.
 */

public class ProfileFragment extends Fragment implements ProfileView {

    private EditText birthDate;
    private User user;
    private ProfilePresenter profilePresenter;
    private EditText name;
    private Button submit;
   // private ProgressBar progressBar;
    private ProgressDialog dialog;
    private ImageView profileImage;
    private TextView fileName;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    private String filePath;
    private TextView male;
    private TextView female;
    private EditText mobileNumber;
    private EditText bio;
    private EditText country;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.profile_fragment, container, false);
        birthDate = (EditText) rootView.findViewById(R.id.birth_date);
        name = (EditText) rootView.findViewById(R.id.name);
        bio = (EditText) rootView.findViewById(R.id.bio);
        country = (EditText) rootView.findViewById(R.id.country);
        profilePresenter = new ProfilePresenter(this);
        profileImage = (ImageView) rootView.findViewById(R.id.userImage);
        fileName = (TextView) rootView.findViewById(R.id.fileName);
        submit = (Button) rootView.findViewById(R.id.submit);
       // progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        dialog = new ProgressDialog(getActivity());
        male = (TextView) rootView.findViewById(R.id.male);
        female = (TextView) rootView.findViewById(R.id.female);
        mobileNumber = (EditText) rootView.findViewById(R.id.mobile);
        birthDateSetUp();
        getuser();
        setSubmitClickListener();
        setImageClickListener();
        setGenderClickListener();
        return rootView;
    }


    private void setGenderClickListener() {
        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male.setTextColor(getResources().getColor(R.color.colorPrimary));
                female.setTextColor(getResources().getColor(R.color.signInGray));
                user.setGender(1);
            }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                female.setTextColor(getResources().getColor(R.color.colorPrimary));
                male.setTextColor(getResources().getColor(R.color.signInGray));
                user.setGender(0);
            }
        });
    }
    private void setSubmitClickListener()
    {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //progressBar.setVisibility(View.VISIBLE);
               // submit.setVisibility(View.GONE);
                dialog.setMessage("Loading");
                dialog.show();
                user.setName(name.getText().toString());
                user.setMobileNumber(mobileNumber.getText().toString());
                user.setCountry(country.getText().toString());
                user.setBio(bio.getText().toString());
                user.setBirthDate(birthDate.getText().toString());
                Log.e("user", user.toString());
                profilePresenter.updateUser(getContext(), user, filePath);
            }
        });
    }

    private void getuser()
    {
        //progressBar.setVisibility(View.VISIBLE);
        //submit.setVisibility(View.GONE);
        dialog.setMessage("Loading");
        dialog.show();
        profilePresenter.getUser(getContext());
    }

    private void birthDateSetUp()
    {
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                birthDate.setText(sdf.format(myCalendar.getTime()));
            }

        };

        birthDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    @Override
    public void setUser(User user2) {
        Log.e("here", user2.getName());
      //  progressBar.setVisibility(View.GONE);
        dialog.dismiss();
       // submit.setVisibility(View.VISIBLE);
        user = user2;
        name.setText(user.getName());
        if(user.getProfileImage() != null)
        {
            Picasso.with(getContext()).load(Constants.BASE_URL + user.getProfileImage()).transform(new CircleTransform())
                    .into(profileImage);
        }

        if(user.getGender() == 1)
        {
            male.setTextColor(getResources().getColor(R.color.colorPrimary));
        }else {
            female.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        mobileNumber.setText(user.getMobileNumber());
        bio.setText(user.getBio());
        country.setText(user.getCountry());
        if(user.getBirthDate() != null) {
            birthDate.setText(user.getBirthDate());
        }


    }

    @Override
    public void error(String message) {

    }

    @Override
    public void success() {

        SharedPreferences.Editor editor = getActivity().getSharedPreferences("myHealth", MODE_PRIVATE).edit();
        editor.putString("email", user.getEmail());
        editor.putInt("type", user.getTypeId());
        editor.putString("name", user.getName());
        editor.putString("image", user.getProfileImage());
        editor.apply();
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    private void setImageClickListener()
    {
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestGalleryPermission();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    if (data != null && data.getData() != null) {
                        Uri selectedImage = data.getData();
                        Log.d("here", "data" + selectedImage);
                        filePath = ImageUploader.getPath(getActivity(), selectedImage);
                    } else if (data != null && data.getExtras() != null && data.getExtras().get("data") != null) {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        Uri selectedImage = ImageUploader.getImageUri(getActivity().getApplicationContext(), photo);
                        Log.d("here", "data" + selectedImage);
                        filePath = ImageUploader.getPath(getActivity(), selectedImage);
                    }
                    // attachmentDisplay.setVisibility(View.VISIBLE);
                    fileName.setText(filePath.substring(filePath.lastIndexOf("/") + 1));
                }

                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedFile = data.getData();
                    filePath = ImageUploader.getPath(getActivity(), selectedFile);
                    Log.d("here", "file path " + filePath);
                    fileName.setText(filePath.substring(filePath.lastIndexOf("/") + 1));
                }
                break;
        }
    }

    private void requestGalleryPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
            openFilePicker();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openFilePicker();
        }
    }

    private void openFilePicker() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickPhoto.setType("image/*");
            startActivityForResult(pickPhoto, 1);
        } else {
            Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickPhoto.setType("image/*");
            startActivityForResult(pickPhoto, 1);
        }
    }
}
