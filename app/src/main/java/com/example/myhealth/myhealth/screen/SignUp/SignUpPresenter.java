package com.example.myhealth.myhealth.screen.SignUp;

import android.content.Context;
import android.util.Log;

import com.example.myhealth.myhealth.api.Network;
import com.example.myhealth.myhealth.api.models.Speciality;
import com.example.myhealth.myhealth.api.models.User;
import com.example.myhealth.myhealth.api.responses.SignInResponse;
import com.example.myhealth.myhealth.api.responses.SignUpResponse;
import com.example.myhealth.myhealth.api.responses.SpecialityListResponse;
import com.example.myhealth.myhealth.api.services.SpecialityService;
import com.example.myhealth.myhealth.api.services.UserService;
import com.example.myhealth.myhealth.screen.Constants;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mayada Fahim on 4/29/2018.
 */

public class SignUpPresenter {

    private SignUpView mView;

    public SignUpPresenter(SignUpView signUpView) {
        mView = signUpView;
    }

    public void signUp(User user, String file, Context context) {
        if (file != null) {
            uploadFile(user, file, context );
        } else {
            sendUser(user);
        }

    }

    private void uploadFile(final User user, String filePath, Context context) {
        final File file = new File(filePath);

        if (validateFileSize(file.length())) {


            Ion.with(context)
                    .load(Constants.UPLOAD_URL)
                    .setMultipartFile("file", "*", file)
                    .asJsonObject().
                    setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            Log.e("here", result.get("url").toString().replaceAll("\"", ""));
                            user.setImage(result.get("url").toString().replaceAll("\"", ""));
                            sendUser(user);
                        }


                    });


        }
    }

    private Boolean validateFileSize(Long fileLength) {
        long fileSizeInMB = fileLength / 1048576;
        return fileSizeInMB <= 10;
    }

    private void sendUser(User user) {
        UserService service = Network.getInstance().createService(UserService.class);

        service.signUp(user.getName(), user.getEmail(), user.getPassword(), user.getMobileNumber(), user.getGender(),
                user.getImage(), user.getSpecialityId(), user.getTypeId(), user.getIdNumber()).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                SignUpResponse responseBody = response.body();
                if (responseBody != null) {
                    int error = responseBody.getError();
                    Log.e("here", error + "");
                    if (error == 1) {
                        mView.error(responseBody.getReasons().get(0));
                    } else {
                        mView.success();
                    }
                }

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void getSpecialities() {
        SpecialityService service = Network.getInstance().createService(SpecialityService.class);

        service.getSpecialities().enqueue(new Callback<SpecialityListResponse>() {
            @Override
            public void onResponse(Call<SpecialityListResponse> call, Response<SpecialityListResponse> response) {
                SpecialityListResponse responseBody = response.body();
                List<Speciality> specialities = new ArrayList<Speciality>();
                if (responseBody != null) {
                    if (responseBody != null) {
                        specialities = responseBody.getSpecialities();
                        Log.e("here", specialities.toString());
                    }
                }
                mView.setSpecialities(specialities);

            }

            @Override
            public void onFailure(Call<SpecialityListResponse> call, Throwable t) {

            }
        });
    }
}
