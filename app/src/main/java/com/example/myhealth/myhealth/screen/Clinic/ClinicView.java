package com.example.myhealth.myhealth.screen.Clinic;

import com.example.myhealth.myhealth.api.models.Category;
import com.example.myhealth.myhealth.api.models.Disease;

import java.util.List;

/**
 * Created by Mayada Fahim on 4/22/2018.
 */

public interface ClinicView {

    public void setCategories(List<Category> categories);
    public void error(String message);
    public void success();
}
