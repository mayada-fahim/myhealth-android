package com.example.myhealth.myhealth.api.models;

/**
 * Created by Hossam Elsawy on 30/05/18.
 */
public class DummyModelDetailsCalories {
    private String name;
    private String Details;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

}
