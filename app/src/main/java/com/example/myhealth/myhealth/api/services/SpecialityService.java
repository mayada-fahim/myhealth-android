package com.example.myhealth.myhealth.api.services;

import com.example.myhealth.myhealth.api.responses.ListDiseasesResponse;
import com.example.myhealth.myhealth.api.responses.SpecialityListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Mayada Fahim on 4/30/2018.
 */

public interface SpecialityService {

    @GET("specialities")
    Call<SpecialityListResponse> getSpecialities();
}
